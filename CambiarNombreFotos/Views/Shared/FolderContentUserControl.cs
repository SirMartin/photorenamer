﻿using System.Globalization;
using PiribiPhotoTools.Helpers;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Shared
{
    public partial class FolderContentUserControl : LocalizableUserControl
    {
        public FolderContentUserControl()
        {
            InitializeComponent();

            CleanLabels();
        }

        private void CleanLabels()
        {
            lblContent.Text = string.Empty;
            lblLine1.Text = string.Empty;
            lblLine2.Text = string.Empty;
        }

        public int ImagesAmount { get; set; }
        public int VideosAmount { get; set; }

        public void CalculateContent(string path, bool isRecursive = false)
        {
            CleanLabels();
            Label selectedLine = this.lblLine1;
            var imagesNumber = FileHelper.GetImageFilesFrom(path, isRecursive).ToArray().Length;
            if (imagesNumber > 0)
            {
                UpdateImages(imagesNumber, selectedLine);
                selectedLine = this.lblLine2;
            }
            var videosNumber = FileHelper.GetVideoFilesFrom(path, isRecursive).ToArray().Length;
            if (videosNumber > 0)
                UpdateVideos(videosNumber, selectedLine);

            if (imagesNumber > 0 || videosNumber > 0)
                this.lblContent.Text = LocalResourceManager.GetString("lblContent.Text", CultureInfo.CurrentCulture);
        }

        private void UpdateImages(int images, Label selectedLine)
        {
            // Images
            ImagesAmount = images;

            UpdateImages(selectedLine);
        }

        private void UpdateImages(Label selectedLine)
        {
            selectedLine.Text = string.Empty;

            // Images
            if (ImagesAmount > 0)
            {
                selectedLine.Text = $"{ImagesAmount} {LocalResourceManager.GetString("pictures_text", CultureInfo.CurrentCulture)}";
            }

            if (ImagesAmount == 1)
            {
                selectedLine.Text = $"{ImagesAmount} {LocalResourceManager.GetString("picture_text", CultureInfo.CurrentCulture)}";
            }
        }

        private void UpdateVideos(int videos, Label selectedLine)
        {
            // Videos
            VideosAmount = videos;

            UpdateVideos(selectedLine);
        }

        private void UpdateVideos(Label selectedLine)
        {
            selectedLine.Text = string.Empty;

            // Videos
            if (VideosAmount > 0)
            {
                selectedLine.Text = $"{VideosAmount} {LocalResourceManager.GetString("videos_text", CultureInfo.CurrentCulture)}";
            }

            if (VideosAmount == 1)
            {
                selectedLine.Text = $"{VideosAmount} {LocalResourceManager.GetString("video_text", CultureInfo.CurrentCulture)}";
            }
        }
    }
}
