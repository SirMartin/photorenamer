﻿using System;
using System.IO;
using System.Windows.Forms;
using Ookii.Dialogs.WinForms;

namespace PiribiPhotoTools.Views.Shared
{
    public partial class FolderBrowserUserControl : UserControl
    {
        public FolderBrowserUserControl()
        {
            InitializeComponent();
        }

        public string SelectedFolder { get; set; }

        public event EventHandler FolderChanged;

        private void RaiseFolderChanged()
        {
            //Null check makes sure the main page is attached to the event
            if (this.FolderChanged != null)
            {
                this.FolderChanged(this, new EventArgs());
            }
        }

        //https://stackoverflow.com/questions/8403086/long-path-with-ellipsis-in-the-middle
        private void SetDirectoryLabel(string path)
        {
            const int maxChars = 40;

            if (path.Length <= maxChars)
            {
                lblDirectory.Text = path;
                return;
            }

            var pathParts = path.Split(Path.DirectorySeparatorChar);

            var trimmedText = "";
            if (pathParts.Length == 2)
            {
                // Trim the last one.
                trimmedText = string.Concat(pathParts[0], Path.DirectorySeparatorChar, pathParts[1].Substring(0, maxChars - pathParts[0].Length - 3), "...");
            }
            else
            {
                // Find last '\' character
                var i = path.LastIndexOf('\\');

                var tokenLeft = "";
                var tokenDrive = path.Substring(0, 3);
                var tokenCenter = @"\...";
                var tokenRight = path.Substring(i, path.Length - i);
                if (tokenRight.Length + tokenCenter.Length >= maxChars - 3)
                {
                    // Substring the start of tokenRight because is bigger than can be.
                    tokenRight = tokenRight.Substring((tokenRight.Length + tokenCenter.Length) - maxChars + 3);

                    // TODO: Improve this part, to show something of the first level if possible.
                    tokenLeft = path.Substring(3, maxChars - 3 - (tokenRight.Length + tokenCenter.Length)) + "...";
                }
                else
                {
                    tokenLeft = path.Substring(3, maxChars - 3 - (tokenRight.Length + tokenCenter.Length)) + "...";
                }

                trimmedText = tokenDrive + tokenLeft + tokenCenter + tokenRight;
            }

            lblDirectory.Text = trimmedText;
            new ToolTip().SetToolTip(lblDirectory, path);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Old folder browser system.
            if (VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                var vistaFolderBrowserDialog = new VistaFolderBrowserDialog();
                var result = vistaFolderBrowserDialog.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    SelectedFolder = vistaFolderBrowserDialog.SelectedPath;
                    SetDirectoryLabel(SelectedFolder);

                    // Load number of files labels.
                    RaiseFolderChanged();
                }
            }
            else
            {
                folderBrowserDialog.ShowNewFolderButton = false;
                folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

                DialogResult result = folderBrowserDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    // the code here will be executed if the user presses Open in
                    // the dialog.
                    SelectedFolder = folderBrowserDialog.SelectedPath;
                    SetDirectoryLabel(SelectedFolder);

                    // Load number of files labels.
                    RaiseFolderChanged();
                }
            }
        }
    }
}
