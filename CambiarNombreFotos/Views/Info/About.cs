﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using NLog;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;
using PiribiPhotoTools.Properties;

namespace PiribiPhotoTools.Views.Info
{
    public partial class About : LocalizableForm, IPhotoToolsForm
    {
        public About()
        {
            InitializeComponent();

            try
            {
                var version = AssemblyName.GetAssemblyName("PiribiPhotoTools.exe").Version;
                lblAbout.Text = string.Concat("v", version);
            }
            catch (FileNotFoundException ex)
            {
                lblAbout.Text = string.Concat("v", new Version(Resources.Version));
                Logger.Error("Cannot find the exe file of the program");
                Logger.Error("Error Exception", ex);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var sInfo = new ProcessStartInfo("https://www.buymeacoffee.com/SirMartin");
            Process.Start(sInfo);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }
        
        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Default;
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
