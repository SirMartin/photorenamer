﻿using NLog;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Info
{
    public partial class Settings : LocalizableForm, IPhotoToolsForm
    {
        public Settings()
        {
            InitializeComponent();

            this.chkAutoUpdates.Checked = Properties.Settings.Default.AutoUpdate;
        }
        
        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }

        private void btnSaveSettings_Click(object sender, System.EventArgs e)
        {
            // Update settings.
            Properties.Settings.Default.AutoUpdate = this.chkAutoUpdates.Checked;
            Properties.Settings.Default.Save();

            this.Close();
        }
    }
}
