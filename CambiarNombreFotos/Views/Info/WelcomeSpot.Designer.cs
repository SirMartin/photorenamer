﻿namespace PiribiPhotoTools.Views.Info
{
    partial class WelcomeSpot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WelcomeSpot));
            this.panWelcomeText = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.panWelcomeText.SuspendLayout();
            this.SuspendLayout();
            // 
            // panWelcomeText
            // 
            this.panWelcomeText.Controls.Add(this.lblTitle);
            this.panWelcomeText.Controls.Add(this.lblText);
            resources.ApplyResources(this.panWelcomeText, "panWelcomeText");
            this.panWelcomeText.Name = "panWelcomeText";
            // 
            // lblTitle
            // 
            resources.ApplyResources(this.lblTitle, "lblTitle");
            this.lblTitle.Name = "lblTitle";
            // 
            // lblText
            // 
            resources.ApplyResources(this.lblText, "lblText");
            this.lblText.Name = "lblText";
            // 
            // WelcomeSpot
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panWelcomeText);
            this.Name = "WelcomeSpot";
            this.panWelcomeText.ResumeLayout(false);
            this.panWelcomeText.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panWelcomeText;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblText;
    }
}