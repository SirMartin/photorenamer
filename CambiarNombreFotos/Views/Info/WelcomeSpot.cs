﻿using NLog;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Info
{
    public partial class WelcomeSpot : LocalizableForm, IPhotoToolsForm
    {
        public WelcomeSpot()
        {
            InitializeComponent();

            Logger.Debug("View Loaded");
        }

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);

            Logger.Debug("Language Changed to {0}", lang);
        }
    }
}
