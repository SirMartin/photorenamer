﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Basic
{
    public partial class DuplicateFiles : LocalizableForm, IPhotoToolsForm
    {
        public DuplicateFiles()
        {
            InitializeComponent();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        #region Properties

        private Dictionary<string, string> DuplicatedFiles { get; set; }

        #endregion

        private void btnFindDuplicates_Click(object sender, EventArgs e)
        {
            // Reset the progress bar.
            progressBar.Value = 0;
            txtInfo.Text = string.Empty;

            // Make the name changes.
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("btnFindDuplicates_Click_WarningText", CultureInfo.CurrentCulture), LocalResourceManager.GetString("btnFindDuplicates_Click_WarningTitle", CultureInfo.CurrentCulture), MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {
                btnFindDuplicates.Enabled = false;
                bwFindDuplicates.RunWorkerAsync();
            }
        }

        private void btnRemoveDuplicates_Click(object sender, EventArgs e)
        {
            // Reset the progress bar.
            progressBar.Value = 0;

            DialogResult dialogResult = MessageBox.Show(LocalResourceManager.GetString("btnRemoveDuplicates_Click_WarningText", CultureInfo.CurrentCulture), LocalResourceManager.GetString("btnRemoveDuplicates_Click_WarningTitle", CultureInfo.CurrentCulture), MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                // Make the name changes.
                if (DuplicatedFiles == null || DuplicatedFiles.Count == 0)
                {
                    // Error directory is empty.
                    MessageBox.Show(LocalResourceManager.GetString("btnRemoveDuplicates_Click_ErrorText", CultureInfo.CurrentCulture), LocalResourceManager.GetString("btnRemoveDuplicates_Click_ErrorTitle", CultureInfo.CurrentCulture), MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                else
                {
                    btnFindDuplicates.Enabled = false;
                    btnRemoveDuplicates.Enabled = false;
                    bwRemoveDuplicates.RunWorkerAsync();
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                //do nothing
            }            
        }

        public static string CreateFileMd5(string filePath)
        {
            using (var md5 = MD5.Create())
            {
                if (FileHelper.IsMultimediaFile(filePath))
                {
                    // hash contents
                    var contentBytes = File.ReadAllBytes(filePath);

                    md5.TransformBlock(contentBytes, 0, contentBytes.Length, contentBytes, 0);
                }

                //Handles empty filePaths case
                md5.TransformFinalBlock(new byte[0], 0, 0);

                return BitConverter.ToString(md5.Hash).Replace("-", "").ToLower();
            }
        }

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }

        #region Find Duplicates Background Work

        private void bwFindDuplicates_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = FileHelper.GetMultimediaFiles(folderBrowserUserControl.SelectedFolder);

            var totalFiles = files.Count();
            int num = 1;

            DuplicatedFiles = new Dictionary<string, string>();
            foreach (var file in files)
            {
                var fileHash = CreateFileMd5(file);
                DuplicatedFiles.Add(file, fileHash);
                
                // Report progress.
                bwFindDuplicates.ReportProgress((int)(((double)num / totalFiles) * 100));

                num++;
            }                        
        }

        private void bwFindDuplicates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwFindDuplicates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Check same hashes.
            var duplicatedHashes = DuplicatedFiles.Values.GroupBy(x => x).Where(x => x.Count() > 1);

            foreach (var item in duplicatedHashes)
            {
                var duplicates = DuplicatedFiles.Where(x => x.Value == item.Key).Select(x => x.Key.Replace(folderBrowserUserControl.SelectedFolder, string.Empty));
                var duplicateText = string.Join(",", duplicates);
                txtInfo.Text += $"{LocalResourceManager.GetString("DuplicateFiles_bwFindDuplicates_RunWorkerCompleted_InfoMessage", CultureInfo.CurrentCulture)} {duplicateText}\r\n";
            }

            // Enable remove buttons if have duplicates.
            btnRemoveDuplicates.Enabled = duplicatedHashes.Any();

            btnFindDuplicates.Enabled = true;

            lblNumberDuplicates.Text = $"{duplicatedHashes.Count()} files duplicates";

            MessageBox.Show(LocalResourceManager.GetString("bwFindDuplicates_RunWorkerCompleted_DuplicatesFoundSuccessfully_Text", CultureInfo.CurrentCulture), LocalResourceManager.GetString("bwFindDuplicates_RunWorkerCompleted_DuplicatesFoundSuccessfully_Title", CultureInfo.CurrentCulture), MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
        }

        #endregion

        #region Remove Duplicates Background Work

        private void bwRemoveDuplicates_DoWork(object sender, DoWorkEventArgs e)
        {
            var duplicatedHashes = DuplicatedFiles.Values.GroupBy(x => x).Where(x => x.Count() > 1);

            var totalElements = duplicatedHashes.Count();
            int num = 1;

            foreach (var item in duplicatedHashes)
            {
                var duplicatesToRemove = DuplicatedFiles.OrderByDescending(x => x.Key).Where(x => x.Value == item.Key).Select(x => x.Key).Skip(1);
                foreach (var duplicatePath in duplicatesToRemove)
                {
                    File.Delete(duplicatePath);
                }
                              
                // Report progress.
                bwRemoveDuplicates.ReportProgress((int)(((double)num / totalElements) * 100));

                num++;
            }
        }

        private void bwRemoveDuplicates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwRemoveDuplicates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnFindDuplicates.Enabled = true;

            MessageBox.Show(LocalResourceManager.GetString("bwRemoveDuplicates_RunWorkerCompleted_DuplicatesRemovedSuccessfully_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwRemoveDuplicates_RunWorkerCompleted_DuplicatesRemovedSuccessfully_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK,
                               MessageBoxIcon.Information);

            // Reload the number of files in the folder after removing duplicates.
            LoadNumberOfFilesLabels();
            lblNumberDuplicates.Text = string.Empty;
        }

        #endregion

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }
    }
}
