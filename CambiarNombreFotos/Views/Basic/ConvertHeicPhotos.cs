﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;
using ImageMagick;
using NLog;

namespace PiribiPhotoTools.Views.Basic
{
    public partial class ConvertHeicPhotos : LocalizableForm, IPhotoToolsForm
    {
        private readonly MetadataHelper _metadataHelper;

        public ConvertHeicPhotos()
        {
            InitializeComponent();

            _metadataHelper = new MetadataHelper();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        #region Progress Bar Change Names

        private void bwConvertImages_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = GetDirectoryFiles(folderBrowserUserControl.SelectedFolder);

            var num = 0;
            var totalFiles = files.Count();

            foreach (var file in files)
            {
                num++;

                try
                {
                    using (var image = new MagickImage(file))
                    {
                        var filename = Path.GetFileName(file);
                        var fileInfo = new FileInfo(file);
                        var newFilename = fileInfo.Name.ToLower().Replace(".HEIC".ToLower(), ".jpg");
                        image.Write(file.Replace(filename, newFilename));
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Error Converting Image {0}/{1}", num, totalFiles);
                    Logger.Error("Error Exception", ex.Message);

                    // File cannot be converted.
                    MessageBox.Show(string.Format(LocalResourceManager.GetString("bwConvertImages_DoWork_ConvertFileError_Text", CultureInfo.CurrentCulture), file),
                        LocalResourceManager.GetString("bwConvertImages_DoWork_ConvertFileError_Title", CultureInfo.CurrentCulture),
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                // Report progress.
                bwConvertImages.ReportProgress(num * 100 / totalFiles);
            }
        }

        private void bwConvertImages_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwConvertImages_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwConvertImages_RunWorkerCompleted_Success_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwConvertImages_RunWorkerCompleted_Success_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        #region Inits

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        #endregion

        #region Events

        private void btnConvert_Click(object sender, EventArgs e)
        {
            // Reset the progress bar.
            progressBar.Value = 0;

            // Make the name changes.
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("ConvertHeicPhotos_btnConvert_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("ConvertHeicPhotos_btnConvert_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bwConvertImages.RunWorkerAsync();
            }
        }

        private IEnumerable<string> GetDirectoryFiles(string directoryPath)
        {
            var pathList = new List<string>();
            pathList = Directory.GetFiles(directoryPath).ToList();
            return pathList.Where(p => FileHelper.IsHeicImageFile(p));
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
