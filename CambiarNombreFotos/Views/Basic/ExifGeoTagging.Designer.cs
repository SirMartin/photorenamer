﻿namespace PiribiPhotoTools.Views.Basic
{
    partial class ExifGeoTagging
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExifGeoTagging));
            this.bwGeoTag = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLongSec = new System.Windows.Forms.TextBox();
            this.txtLongMin = new System.Windows.Forms.TextBox();
            this.txtLongDeg = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLatSec = new System.Windows.Forms.TextBox();
            this.txtLatMin = new System.Windows.Forms.TextBox();
            this.lblLocation = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.lblLongitude = new System.Windows.Forms.Label();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.lblLatitude = new System.Windows.Forms.Label();
            this.txtLatDeg = new System.Windows.Forms.TextBox();
            this.btnGeoTag = new System.Windows.Forms.Button();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bwGeoTag
            // 
            this.bwGeoTag.WorkerReportsProgress = true;
            this.bwGeoTag.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwGeoTag_DoWork);
            this.bwGeoTag.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwGeoTag_ProgressChanged);
            this.bwGeoTag.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwGeoTag_RunWorkerCompleted);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtLongSec);
            this.panel1.Controls.Add(this.txtLongMin);
            this.panel1.Controls.Add(this.txtLongDeg);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtLatSec);
            this.panel1.Controls.Add(this.txtLatMin);
            this.panel1.Controls.Add(this.lblLocation);
            this.panel1.Controls.Add(this.txtLocation);
            this.panel1.Controls.Add(this.lblLongitude);
            this.panel1.Controls.Add(this.folderContentUserControl);
            this.panel1.Controls.Add(this.lblLatitude);
            this.panel1.Controls.Add(this.txtLatDeg);
            this.panel1.Controls.Add(this.btnGeoTag);
            this.panel1.Name = "panel1";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // txtLongSec
            // 
            resources.ApplyResources(this.txtLongSec, "txtLongSec");
            this.txtLongSec.Name = "txtLongSec";
            // 
            // txtLongMin
            // 
            resources.ApplyResources(this.txtLongMin, "txtLongMin");
            this.txtLongMin.Name = "txtLongMin";
            // 
            // txtLongDeg
            // 
            resources.ApplyResources(this.txtLongDeg, "txtLongDeg");
            this.txtLongDeg.Name = "txtLongDeg";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // txtLatSec
            // 
            resources.ApplyResources(this.txtLatSec, "txtLatSec");
            this.txtLatSec.Name = "txtLatSec";
            // 
            // txtLatMin
            // 
            resources.ApplyResources(this.txtLatMin, "txtLatMin");
            this.txtLatMin.Name = "txtLatMin";
            // 
            // lblLocation
            // 
            resources.ApplyResources(this.lblLocation, "lblLocation");
            this.lblLocation.Name = "lblLocation";
            // 
            // txtLocation
            // 
            resources.ApplyResources(this.txtLocation, "txtLocation");
            this.txtLocation.Name = "txtLocation";
            // 
            // lblLongitude
            // 
            resources.ApplyResources(this.lblLongitude, "lblLongitude");
            this.lblLongitude.Name = "lblLongitude";
            // 
            // folderContentUserControl
            // 
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.ImagesAmount = 0;
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // lblLatitude
            // 
            resources.ApplyResources(this.lblLatitude, "lblLatitude");
            this.lblLatitude.Name = "lblLatitude";
            // 
            // txtLatDeg
            // 
            resources.ApplyResources(this.txtLatDeg, "txtLatDeg");
            this.txtLatDeg.Name = "txtLatDeg";
            // 
            // btnGeoTag
            // 
            resources.ApplyResources(this.btnGeoTag, "btnGeoTag");
            this.btnGeoTag.Name = "btnGeoTag";
            this.btnGeoTag.UseVisualStyleBackColor = true;
            this.btnGeoTag.Click += new System.EventHandler(this.btnGeoTag_Click);
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            this.progressBar.Name = "progressBar";
            // 
            // ExifGeoTagging
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar);
            this.Icon = global::PiribiPhotoTools.Properties.Resources.photorenamer;
            this.Name = "ExifGeoTagging";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Custom.LabeledProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bwGeoTag;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLatitude;
        private System.Windows.Forms.TextBox txtLatDeg;
        private System.Windows.Forms.Button btnGeoTag;
        private Shared.FolderContentUserControl folderContentUserControl;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
        private System.Windows.Forms.Label lblLongitude;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLatSec;
        private System.Windows.Forms.TextBox txtLatMin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtLongSec;
        private System.Windows.Forms.TextBox txtLongMin;
        private System.Windows.Forms.TextBox txtLongDeg;
    }
}

