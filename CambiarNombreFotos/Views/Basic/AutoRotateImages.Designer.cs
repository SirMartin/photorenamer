﻿namespace PiribiPhotoTools.Views.Basic
{
    partial class AutoRotateImages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoRotateImages));
            this.btnRotateImages = new System.Windows.Forms.Button();
            this.bwAutoRotateImages = new System.ComponentModel.BackgroundWorker();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.elementHost = new System.Windows.Forms.Integration.ElementHost();
            this.label1 = new System.Windows.Forms.Label();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.SuspendLayout();
            // 
            // btnRotateImages
            // 
            this.btnRotateImages.Location = new System.Drawing.Point(378, 138);
            this.btnRotateImages.Name = "btnRotateImages";
            this.btnRotateImages.Size = new System.Drawing.Size(107, 23);
            this.btnRotateImages.TabIndex = 25;
            this.btnRotateImages.Text = "Rotar Imagenes";
            this.btnRotateImages.UseVisualStyleBackColor = true;
            this.btnRotateImages.Click += new System.EventHandler(this.btnRotateImages_Click);
            // 
            // bwAutoRotateImages
            // 
            this.bwAutoRotateImages.WorkerReportsProgress = true;
            this.bwAutoRotateImages.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAutoRotateImages_DoWork);
            this.bwAutoRotateImages.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwAutoRotateImages_ProgressChanged);
            this.bwAutoRotateImages.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAutoRotateImages_RunWorkerCompleted);
            // 
            // progressBar
            // 
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            this.progressBar.Location = new System.Drawing.Point(12, 176);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(492, 23);
            this.progressBar.TabIndex = 26;
            // 
            // elementHost
            // 
            this.elementHost.Location = new System.Drawing.Point(0, 220);
            this.elementHost.Name = "elementHost";
            this.elementHost.Size = new System.Drawing.Size(520, 171);
            this.elementHost.TabIndex = 27;
            this.elementHost.Text = "elementHost";
            this.elementHost.Child = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(458, 39);
            this.label1.TabIndex = 28;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // folderContentUserControl
            // 
            this.folderContentUserControl.ImagesAmount = 0;
            this.folderContentUserControl.Location = new System.Drawing.Point(12, 110);
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.Size = new System.Drawing.Size(162, 60);
            this.folderContentUserControl.TabIndex = 30;
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // folderBrowserUserControl
            // 
            this.folderBrowserUserControl.Location = new System.Drawing.Point(30, 78);
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            this.folderBrowserUserControl.Size = new System.Drawing.Size(434, 31);
            this.folderBrowserUserControl.TabIndex = 31;
            // 
            // AutoRotateImages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(523, 393);
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.folderContentUserControl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.elementHost);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnRotateImages);
            this.Name = "AutoRotateImages";
            this.Text = "AutoRotateImages";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnRotateImages;
        private System.ComponentModel.BackgroundWorker bwAutoRotateImages;
        private Custom.LabeledProgressBar progressBar;
        private System.Windows.Forms.Integration.ElementHost elementHost;
        private System.Windows.Forms.Label label1;
        private Views.Shared.FolderContentUserControl folderContentUserControl;
        private Views.Shared.FolderBrowserUserControl folderBrowserUserControl;
    }
}