﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.WpfElements;
using NLog;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Basic
{
    public partial class AutoRotateImages : LocalizableForm, IPhotoToolsForm
    {
        public AutoRotateImages()
        {
            InitializeComponent();
            
            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();

            // Initialize Image Carousel
            //InitializeImageCarousel();
        }

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        private void InitializeImageCarousel()
        {
            // Initialize de carousel.
            var wpfControl = new ImageCarousel(folderBrowserUserControl.SelectedFolder);
            wpfControl.InitializeComponent();
            elementHost.Child = wpfControl;
        }
        
        private void btnRotateImages_Click(object sender, EventArgs e)
        {
            // Do the work.
            bwAutoRotateImages.RunWorkerAsync();
        }
        
        #region Async Work Auto Rotate Images

        private void bwAutoRotateImages_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = FileHelper.GetImageFilesFrom(folderBrowserUserControl.SelectedFolder, false);
            var totalFiles = files.Count();

            Logger.Info("Founded " + totalFiles + " files in the directory.");

            int i = 1;
            foreach (var filepath in files)
            {
                var progressValue = (i * 100 / totalFiles);

                try
                {
                    // Create the new file path.
                    string newfilepath = Path.Combine(Path.GetDirectoryName(filepath),
                        Path.GetFileNameWithoutExtension(filepath) + "_rotated");// + Path.GetExtension(filepath));

                    //http://stackoverflow.com/questions/6222053/problem-reading-jpeg-metadata-orientation
                    // Detect if the image needs to be rotated or not.
                    bool existsRotation = false;
                    Image image;
                    using (FileStream myStream = new FileStream(filepath, FileMode.Open))
                    {
                        image = Image.FromStream(myStream);

#warning El problema es que los archivos los tiene pillados la galeria, habria que quitarlos de la galeria, o borrar la galeria, o hacer una copia o algo asi.
                        // int id is 274
                        if (!image.PropertyIdList.Any(id => id == 0x112))
                        {
                            // Report progress.
                            bwAutoRotateImages.ReportProgress(progressValue);
                            i++;
                            continue;
                        }

                        var orientation = image.GetPropertyItem(0x112).Value[0];
                        if (orientation == 6)
                        {
                            existsRotation = true;
                            RotateImage(image, RotateFlipType.Rotate90FlipNone, newfilepath);
                        }

                        if (orientation == 8)
                        {
                            existsRotation = true;
                            RotateImage(image, RotateFlipType.Rotate270FlipNone, newfilepath);
                        }
                    }

                    if (existsRotation)
                    {
                        // Delete the previous file.
                        File.Delete(filepath);

                        // Rename the new one.
                        File.Move(newfilepath, filepath);
                    }

                    // Report progress.
                    bwAutoRotateImages.ReportProgress(progressValue);
                    i++;
                }
                catch (Exception ex)
                {
                    Logger.Error("Error processing image number {0}/{1}", i, totalFiles);
                    Logger.Error("Exception of the error: ", ex);
                }
            }
        }

        private void RotateImage(Image image, RotateFlipType rotateFlipType, string newfilepath)
        {
            image.RotateFlip(rotateFlipType);
            image.Save(newfilepath);
            image.Dispose();
        }

        private void bwAutoRotateImages_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
            Logger.Info("Procesando imagenes automaticamente - {0}%", e.ProgressPercentage);
        }

        private void bwAutoRotateImages_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("El ajuste de fechas ha finalizado.", "INFO", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            Logger.Info("Finished Image Processing");

        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
