﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;
using PiribiPhotoTools.Services;

namespace PiribiPhotoTools.Views.Basic
{
    public partial class ExifGeoTagging : LocalizableForm, IPhotoToolsForm
    {
        public ExifGeoTagging()
        {
            InitializeComponent();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);

            Logger.Info("View Loaded");
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        #region Properties

        public string Separator { get; set; }

        #endregion

        #region Progress Bar Change Names

        private void bwGeoTag_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = GetDirectoryFiles(folderBrowserUserControl.SelectedFolder);
            // Create the initial number.
            var num = 1;
            var totalFiles = files.Count();

            // Generate latitude and longitude.
            double latitude;
            double longitude;
            if (txtLocation.Text != "")
            {
                // Extract from Location via an API or something.
                var coordinatesTask = new GeoApifyHelper().GetCoordinates(txtLocation.Text);

                Task.WaitAll(coordinatesTask);

                var coordinates = coordinatesTask.Result;

                if (coordinates == null)
                {
                    // Error directory is empty.
                    MessageBox.Show(LocalResourceManager.GetString("bwGeoTag_DoWork_NoCoordinatesResultError_Text", CultureInfo.CurrentCulture),
                        LocalResourceManager.GetString("bwGeoTag_DoWork_NoCoordinatesResultError_Title", CultureInfo.CurrentCulture),
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return;
                }

                latitude = coordinates.Latitude;
                longitude = coordinates.Longitude;
            }
            else
            {
                var negativeLat = false;
                var negativeLong = false;
                if (txtLatDeg.Text.StartsWith("-"))
                {
                    negativeLat = true;
                    txtLatDeg.Text.Remove(0, 1);
                }

                if (txtLongDeg.Text.StartsWith("-"))
                {
                    negativeLong = true;
                    txtLongDeg.Text.Remove(0, 1);
                }

                latitude = double.Parse(txtLatDeg.Text) + double.Parse(txtLatMin.Text) / 60 + double.Parse(txtLatSec.Text) / 3600;
                longitude = double.Parse(txtLongDeg.Text) + double.Parse(txtLongMin.Text) / 60 + double.Parse(txtLongSec.Text) / 3600;

                if (negativeLat)
                    latitude = latitude * -1;

                if (negativeLong)
                    longitude = longitude * -1;
            }


            foreach (var file in files)
            {
                num++;

                var fileName = Path.GetFileName(file);
                var newFileName = fileName + "_geotagged" + Path.GetExtension(file);

                using (FileStream myStream = new FileStream(file, FileMode.Open))
                {
                    var image = Image.FromStream(myStream);

                    image = Geotag(image, latitude, longitude);

                    image.Save(file.Replace(fileName, newFileName));
                }

                File.Delete(file);
                File.Move(file.Replace(fileName, newFileName), file);

                // Report progress.
                bwGeoTag.ReportProgress((num / totalFiles) * 100);
            }

        }

        private void bwGeoTag_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwGeoTag_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwGeoTag_RunWorkerCompleted_Success_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwGeoTag_RunWorkerCompleted_Success_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        #region Inits

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        #endregion

        #region Events


        private void btnGeoTag_Click(object sender, EventArgs e)
        {
            // Reset the progress bar.
            progressBar.Value = 0;

            // Make the name changes.
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("ExifGeoTagging_btnGeoTag_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("ExifGeoTagging_btnGeoTag_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if ((txtLatDeg.Text != "" && txtLatMin.Text != "" && txtLatSec.Text != "" && txtLongDeg.Text != "" && txtLongMin.Text != "" && txtLongSec.Text != "") || (txtLocation.Text != ""))
            {
                bwGeoTag.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show(LocalResourceManager.GetString("ExifGeoTagging_btnGeoTag_Click_NotLocationProvided_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("ExifGeoTagging_btnGeoTag_Click_NotLocationProvided_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private IEnumerable<string> GetDirectoryFiles(string directoryPath)
        {
            return Directory.GetFiles(directoryPath).Where(p => FileHelper.IsImageFile(p));
        }

        static Image Geotag(Image original, double lat, double lng)
        {
            // These constants come from the CIPA DC-008 standard for EXIF 2.3
            const short ExifTypeByte = 1;
            const short ExifTypeAscii = 2;
            const short ExifTypeRational = 5;

            const int ExifTagGPSVersionID = 0x0000;
            const int ExifTagGPSLatitudeRef = 0x0001;
            const int ExifTagGPSLatitude = 0x0002;
            const int ExifTagGPSLongitudeRef = 0x0003;
            const int ExifTagGPSLongitude = 0x0004;

            char latHemisphere = 'N';
            if (lat < 0)
            {
                latHemisphere = 'S';
                lat = -lat;
            }
            char lngHemisphere = 'E';
            if (lng < 0)
            {
                lngHemisphere = 'W';
                lng = -lng;
            }

            MemoryStream ms = new MemoryStream();
            original.Save(ms, ImageFormat.Jpeg);
            ms.Seek(0, SeekOrigin.Begin);

            Image img = Image.FromStream(ms);
            AddProperty(img, ExifTagGPSVersionID, ExifTypeByte, new byte[] { 2, 3, 0, 0 });
            AddProperty(img, ExifTagGPSLatitudeRef, ExifTypeAscii, new byte[] { (byte)latHemisphere, 0 });
            AddProperty(img, ExifTagGPSLatitude, ExifTypeRational, ConvertToRationalTriplet(lat));
            AddProperty(img, ExifTagGPSLongitudeRef, ExifTypeAscii, new byte[] { (byte)lngHemisphere, 0 });
            AddProperty(img, ExifTagGPSLongitude, ExifTypeRational, ConvertToRationalTriplet(lng));

            return img;
        }

        static byte[] ConvertToRationalTriplet(double value)
        {
            int degrees = (int)Math.Floor(value);
            value = (value - degrees) * 60;
            int minutes = (int)Math.Floor(value);
            value = (value - minutes) * 60 * 100;
            int seconds = (int)Math.Round(value);
            byte[] bytes = new byte[3 * 2 * 4]; // Degrees, minutes, and seconds, each with a numerator and a denominator, each composed of 4 bytes
            int i = 0;
            Array.Copy(BitConverter.GetBytes(degrees), 0, bytes, i, 4); i += 4;
            Array.Copy(BitConverter.GetBytes(1), 0, bytes, i, 4); i += 4;
            Array.Copy(BitConverter.GetBytes(minutes), 0, bytes, i, 4); i += 4;
            Array.Copy(BitConverter.GetBytes(1), 0, bytes, i, 4); i += 4;
            Array.Copy(BitConverter.GetBytes(seconds), 0, bytes, i, 4); i += 4;
            Array.Copy(BitConverter.GetBytes(100), 0, bytes, i, 4);
            return bytes;
        }

        static void AddProperty(Image img, int id, short type, byte[] value)
        {
            PropertyItem pi = img.PropertyItems[0];
            pi.Id = id;
            pi.Type = type;
            pi.Len = value.Length;
            pi.Value = value;
            img.SetPropertyItem(pi);
        }


        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
