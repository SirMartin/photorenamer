﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using NLog;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;
using PiribiPhotoTools.Models;

namespace PiribiPhotoTools.Views.Basic
{
    public partial class RenamePhotos : LocalizableForm, IPhotoToolsForm
    {
        private readonly MetadataHelper _metadataHelper;

        private Random _random;
        
        public RenamePhotos()
        {
            InitializeComponent();

            FillSeparatorDdl();

            InitialValues();

            _metadataHelper = new MetadataHelper();

            _random = new Random();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);

            Logger.Info("View Loaded");
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        #region Properties

        public string Separator { get; set; }

        #endregion

        #region Progress Bar Change Names

        private void bwChangeNames_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = GetDirectoryFiles(folderBrowserUserControl.SelectedFolder);
            // Create the initial number.
            var initialNumber = 1;
            var num = 1;
            if (chkFixStartNumber.Checked)
            {
                initialNumber = Convert.ToInt32(txtInitialNumber.Text);
            }

            var totalFiles = files.Count();
            // Create alternative names dictionary, because we have problems if we change names to a filename that exists before.
            // Solved issue #12 ==> https://bitbucket.org/SirMartin/photorenamer/issues/12/renombrar-fotos-con-mismo-nombre-falla
            var alternativeNames = new Dictionary<string, string>();
            foreach (var file in files)
            {
                var filename = Path.GetFileName(file);
                var newFilename = MakeName(initialNumber, totalFiles) + Path.GetExtension(file);
                num++;
                initialNumber++;

                // Only make change if filename and new filename are distinct.
                if (filename == newFilename)
                    continue;

                if (File.Exists(file.Replace(filename, newFilename)))
                {
                    try
                    {
                        // The new filename exists, we need to change it.
                        File.Move(file.Replace(filename, newFilename), file.Replace(filename, newFilename) + ".tmp");

                        // Add the new filename to the alternative names.
                        alternativeNames.Add(file.Replace(filename, newFilename), file.Replace(filename, newFilename) + ".tmp");
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error Converting Image {0}/{1}", num, totalFiles);
                        Logger.Error("Error Exception", ex);

                        // File cannot be moved.
                        MessageBox.Show(string.Format(LocalResourceManager.GetString("bwChangeNames_DoWork_RenameFileError_Text", CultureInfo.CurrentCulture), file),
                            LocalResourceManager.GetString("bwChangeNames_DoWork_RenameFileError_Title", CultureInfo.CurrentCulture),
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

                // Change the filename to the new name.
                // Check if exists an alternative name for this file.
                var originalFile = file;
                if (alternativeNames.Keys.Any(k => k == file))
                {
                    // Use as original filename the alternative name.
                    originalFile = alternativeNames[file];
                }

                try
                {
                    File.Move(originalFile, file.Replace(filename, newFilename));
                }
                catch (Exception ex)
                {
                    Logger.Error("Error Converting Image {0}/{1}", num, totalFiles);
                    Logger.Error("Error Exception", ex);

                    // File cannot be moved.
                    MessageBox.Show(string.Format(LocalResourceManager.GetString("bwChangeNames_DoWork_RenameFileError_Text", CultureInfo.CurrentCulture), file),
                        LocalResourceManager.GetString("bwChangeNames_DoWork_RenameFileError_Title", CultureInfo.CurrentCulture),
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }


                // Report progress.
                bwChangeNames.ReportProgress((num / totalFiles) * 100);
            }

        }

        private void bwChangeNames_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwChangeNames_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwChangeNames_RunWorkerCompleted_Success_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwChangeNames_RunWorkerCompleted_Success_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        #region Inits

        public Dictionary<string, string> SeparatorChars
        {
            get
            {
                var dic = new Dictionary<string, string>
                {
                    {LocalResourceManager.GetString("nothing_text", CultureInfo.CurrentCulture) ?? "", ""},
                    {LocalResourceManager.GetString("space_text", CultureInfo.CurrentCulture) ?? " ", " "},
                    {LocalResourceManager.GetString("hyphen_text", CultureInfo.CurrentCulture) ?? "-", "-"},
                    {LocalResourceManager.GetString("underscore_text", CultureInfo.CurrentCulture) ?? "_", "_"}
                };
                return dic;
            }
        }

        private void InitialValues()
        {
            // Hide initial number text box.
            txtInitialNumber.Hide();
        }

        private void FillSeparatorDdl()
        {
            ddlSeparator.DataSource = new BindingSource(SeparatorChars, null);
            ddlSeparator.DisplayMember = "Key";
            ddlSeparator.ValueMember = "Value";
        }

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        #endregion

        #region Events

        private void btnChange_Click(object sender, EventArgs e)
        {
            // Reset the progress bar.
            progressBar.Value = 0;

            // Make the name changes.
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("RenamePhotos_btnChange_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("RenamePhotos_btnChange_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!rbnOrderByName.Checked && !rbnOrderByCreation.Checked && !rbnOrderByModified.Checked && !rbnOrderByTakenDate.Checked && !rbnOrderRandom.Checked)
            {
                MessageBox.Show(LocalResourceManager.GetString("RenamePhotos_btnChange_Click_OrderMethodNotSelected_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("RenamePhotos_btnChange_Click_OrderMethodNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (txtName.Text == "")
            {
                MessageBox.Show(LocalResourceManager.GetString("RenamePhotos_btnChange_Click_NameIsEmpty_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("RenamePhotos_btnChange_Click_NameIsEmpty_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bwChangeNames.RunWorkerAsync();
            }
        }

        private IEnumerable<string> GetDirectoryFiles(string directoryPath)
        {          
            var pathList = new List<string>();
            if (rbnOrderByCreation.Checked)
            {
                var di = new DirectoryInfo(directoryPath);
                var files = di.GetFileSystemInfos();
                var orderedFiles = files.OrderBy(f => f.CreationTime);
                pathList = orderedFiles.Select(f => f.FullName).ToList();
            }
            else if (rbnOrderByModified.Checked)
            {
                var di = new DirectoryInfo(directoryPath);
                var files = di.GetFileSystemInfos();
                var orderedFiles = files.OrderBy(f => f.LastWriteTime);
                pathList = orderedFiles.Select(f => f.FullName).ToList();
            }
            else if (rbnOrderByTakenDate.Checked)
            {
                var files = Directory.GetFiles(directoryPath);
                var photoList = new List<PhotoToOrder>();
                foreach (var file in files)
                {
                    photoList.Add(new PhotoToOrder()
                    {
                        DateTime = MetadataHelper.GetMetadataDate(file),
                        Filepath = file
                    });
                }

                // Set last one, all the files without taken datetime.
                photoList.Where(f => !f.DateTime.HasValue).ToList().ForEach(f => f.DateTime = DateTime.MaxValue);

                pathList = photoList.OrderBy(f => f.DateTime).Select(f => f.Filepath).ToList();
            }
            else if (rbnOrderByName.Checked)
            {
                pathList = Directory.GetFiles(directoryPath).OrderBy(x => x).ToList();
            }
            else if (rbnOrderRandom.Checked)
            {
                var files = Directory.GetFiles(directoryPath).ToList();

                while (files.Count > 0)
                {
                    var randomValue = _random.Next(0, files.Count - 1);

                    pathList.Add(files[randomValue]);

                    files.RemoveAt(randomValue);
                }
            }

            return pathList.Where(p => FileHelper.IsMultimediaFile(p));
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            string example;
            if (chkFixStartNumber.Checked)
            {
                var initialNumber = 1;
                // Use the specified initial number instead of 1.
                if (int.TryParse(txtInitialNumber.Text, out var initialNumberParsed))
                    initialNumber = initialNumberParsed;
                example = MakeName(initialNumber, 100);
            }
            else
            {
                example = MakeName(1, 100);
            }
            lblExampleName.Text = example;
        }

        private string GetNumWithFormat(int total, int? num = null)
        {
            var result = num.ToString();
            var totalLength = total.ToString().Length;

            while (result.Length < totalLength)
            {
                result = "0" + result;
            }

            return result;
        }

        private string MakeName(int num, int total)
        {
            var text = txtName.Text;

            string number;
            number = GetNumWithFormat(total, num);

            var newName = text + Separator + number;

            return newName;
        }

        private void ddlSeparator_SelectedValueChanged(object sender, EventArgs e)
        {
            Separator = ddlSeparator.SelectedValue.ToString();
            string example;
            if (chkFixStartNumber.Checked)
            {
                // Use the specified initial number instead of 1.
                var initialNumber = Convert.ToInt32(txtInitialNumber.Text);
                example = MakeName(initialNumber, 100);
            }
            else
            {
                example = MakeName(1, 100);
            }
            lblExampleName.Text = example;
        }

        private void chkFixStartNumber_CheckedChanged(object sender, EventArgs e)
        {
            // Set visible only if checkbox its selected.
            txtInitialNumber.Visible = chkFixStartNumber.Checked;
        }

        #endregion

        #region Utilidades

        /// <summary>
        /// Only must introduce positive numbers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onlyNumericInTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Force to only use numeric keys.
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar)) // Accept contro keys, fx: delete
            {
                e.Handled = false;
            }
            else
            {
                // Rest of keys are deactivated.
                e.Handled = true;
            }
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
