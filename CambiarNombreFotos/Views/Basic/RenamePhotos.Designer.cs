﻿namespace PiribiPhotoTools.Views.Basic
{
    partial class RenamePhotos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenamePhotos));
            this.bwChangeNames = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.txtInitialNumber = new System.Windows.Forms.TextBox();
            this.chkFixStartNumber = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbnOrderRandom = new System.Windows.Forms.RadioButton();
            this.rbnOrderByName = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.rbnOrderByTakenDate = new System.Windows.Forms.RadioButton();
            this.rbnOrderByModified = new System.Windows.Forms.RadioButton();
            this.rbnOrderByCreation = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.ddlSeparator = new System.Windows.Forms.ComboBox();
            this.btnChange = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblExampleName = new System.Windows.Forms.Label();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bwChangeNames
            // 
            this.bwChangeNames.WorkerReportsProgress = true;
            this.bwChangeNames.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwChangeNames_DoWork);
            this.bwChangeNames.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwChangeNames_ProgressChanged);
            this.bwChangeNames.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwChangeNames_RunWorkerCompleted);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.folderContentUserControl);
            this.panel1.Controls.Add(this.txtInitialNumber);
            this.panel1.Controls.Add(this.chkFixStartNumber);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.ddlSeparator);
            this.panel1.Controls.Add(this.btnChange);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblExampleName);
            this.panel1.Name = "panel1";
            // 
            // folderContentUserControl
            // 
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.ImagesAmount = 0;
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // txtInitialNumber
            // 
            resources.ApplyResources(this.txtInitialNumber, "txtInitialNumber");
            this.txtInitialNumber.Name = "txtInitialNumber";
            this.txtInitialNumber.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            this.txtInitialNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumericInTextbox_KeyPress);
            // 
            // chkFixStartNumber
            // 
            resources.ApplyResources(this.chkFixStartNumber, "chkFixStartNumber");
            this.chkFixStartNumber.Name = "chkFixStartNumber";
            this.chkFixStartNumber.UseVisualStyleBackColor = true;
            this.chkFixStartNumber.CheckedChanged += new System.EventHandler(this.chkFixStartNumber_CheckedChanged);
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Controls.Add(this.rbnOrderRandom);
            this.panel3.Controls.Add(this.rbnOrderByName);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.rbnOrderByTakenDate);
            this.panel3.Controls.Add(this.rbnOrderByModified);
            this.panel3.Controls.Add(this.rbnOrderByCreation);
            this.panel3.Name = "panel3";
            // 
            // rbnOrderRandom
            // 
            resources.ApplyResources(this.rbnOrderRandom, "rbnOrderRandom");
            this.rbnOrderRandom.Name = "rbnOrderRandom";
            this.rbnOrderRandom.UseVisualStyleBackColor = true;
            // 
            // rbnOrderByName
            // 
            resources.ApplyResources(this.rbnOrderByName, "rbnOrderByName");
            this.rbnOrderByName.Name = "rbnOrderByName";
            this.rbnOrderByName.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // rbnOrderByTakenDate
            // 
            resources.ApplyResources(this.rbnOrderByTakenDate, "rbnOrderByTakenDate");
            this.rbnOrderByTakenDate.Name = "rbnOrderByTakenDate";
            this.rbnOrderByTakenDate.UseVisualStyleBackColor = true;
            // 
            // rbnOrderByModified
            // 
            resources.ApplyResources(this.rbnOrderByModified, "rbnOrderByModified");
            this.rbnOrderByModified.Name = "rbnOrderByModified";
            this.rbnOrderByModified.UseVisualStyleBackColor = true;
            // 
            // rbnOrderByCreation
            // 
            resources.ApplyResources(this.rbnOrderByCreation, "rbnOrderByCreation");
            this.rbnOrderByCreation.Name = "rbnOrderByCreation";
            this.rbnOrderByCreation.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // ddlSeparator
            // 
            resources.ApplyResources(this.ddlSeparator, "ddlSeparator");
            this.ddlSeparator.FormattingEnabled = true;
            this.ddlSeparator.Name = "ddlSeparator";
            this.ddlSeparator.SelectedValueChanged += new System.EventHandler(this.ddlSeparator_SelectedValueChanged);
            // 
            // btnChange
            // 
            resources.ApplyResources(this.btnChange, "btnChange");
            this.btnChange.Name = "btnChange";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lblExampleName
            // 
            resources.ApplyResources(this.lblExampleName, "lblExampleName");
            this.lblExampleName.Name = "lblExampleName";
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            this.progressBar.Name = "progressBar";
            // 
            // RenamePhotos
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar);
            this.Icon = global::PiribiPhotoTools.Properties.Resources.photorenamer;
            this.Name = "RenamePhotos";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Custom.LabeledProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bwChangeNames;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbnOrderByName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbnOrderByTakenDate;
        private System.Windows.Forms.RadioButton rbnOrderByModified;
        private System.Windows.Forms.RadioButton rbnOrderByCreation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.ComboBox ddlSeparator;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblExampleName;
        private System.Windows.Forms.CheckBox chkFixStartNumber;
        private System.Windows.Forms.TextBox txtInitialNumber;
        private Shared.FolderContentUserControl folderContentUserControl;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
        private System.Windows.Forms.RadioButton rbnOrderRandom;
    }
}

