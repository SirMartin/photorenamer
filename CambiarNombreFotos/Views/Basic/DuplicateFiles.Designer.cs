﻿namespace PiribiPhotoTools.Views.Basic
{
    partial class DuplicateFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DuplicateFiles));
            this.btnFindDuplicates = new System.Windows.Forms.Button();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.btnRemoveDuplicates = new System.Windows.Forms.Button();
            this.bwFindDuplicates = new System.ComponentModel.BackgroundWorker();
            this.bwRemoveDuplicates = new System.ComponentModel.BackgroundWorker();
            this.lblNumberDuplicates = new System.Windows.Forms.Label();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.SuspendLayout();
            // 
            // btnFindDuplicates
            // 
            resources.ApplyResources(this.btnFindDuplicates, "btnFindDuplicates");
            this.btnFindDuplicates.Name = "btnFindDuplicates";
            this.btnFindDuplicates.UseVisualStyleBackColor = true;
            this.btnFindDuplicates.Click += new System.EventHandler(this.btnFindDuplicates_Click);
            // 
            // txtInfo
            // 
            resources.ApplyResources(this.txtInfo, "txtInfo");
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            // 
            // btnRemoveDuplicates
            // 
            resources.ApplyResources(this.btnRemoveDuplicates, "btnRemoveDuplicates");
            this.btnRemoveDuplicates.Name = "btnRemoveDuplicates";
            this.btnRemoveDuplicates.UseVisualStyleBackColor = true;
            this.btnRemoveDuplicates.Click += new System.EventHandler(this.btnRemoveDuplicates_Click);
            // 
            // bwFindDuplicates
            // 
            this.bwFindDuplicates.WorkerReportsProgress = true;
            this.bwFindDuplicates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwFindDuplicates_DoWork);
            this.bwFindDuplicates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwFindDuplicates_ProgressChanged);
            this.bwFindDuplicates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwFindDuplicates_RunWorkerCompleted);
            // 
            // bwRemoveDuplicates
            // 
            this.bwRemoveDuplicates.WorkerReportsProgress = true;
            this.bwRemoveDuplicates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwRemoveDuplicates_DoWork);
            this.bwRemoveDuplicates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwRemoveDuplicates_ProgressChanged);
            this.bwRemoveDuplicates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwRemoveDuplicates_RunWorkerCompleted);
            // 
            // lblNumberDuplicates
            // 
            resources.ApplyResources(this.lblNumberDuplicates, "lblNumberDuplicates");
            this.lblNumberDuplicates.Name = "lblNumberDuplicates";
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            this.progressBar.Name = "progressBar";
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // folderContentUserControl
            // 
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.ImagesAmount = 0;
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // DuplicateFiles
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblNumberDuplicates);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.folderContentUserControl);
            this.Controls.Add(this.btnRemoveDuplicates);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.btnFindDuplicates);
            this.Name = "DuplicateFiles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFindDuplicates;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Button btnRemoveDuplicates;
        private Shared.FolderContentUserControl folderContentUserControl;
        private System.ComponentModel.BackgroundWorker bwFindDuplicates;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
        private Custom.LabeledProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bwRemoveDuplicates;
        private System.Windows.Forms.Label lblNumberDuplicates;
    }
}