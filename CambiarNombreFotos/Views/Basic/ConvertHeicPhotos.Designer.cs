﻿namespace PiribiPhotoTools.Views.Basic
{
    partial class ConvertHeicPhotos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConvertHeicPhotos));
            this.bwConvertImages = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.btnConvert = new System.Windows.Forms.Button();
            this.lblDescription = new System.Windows.Forms.Label();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bwConvertImages
            // 
            this.bwConvertImages.WorkerReportsProgress = true;
            this.bwConvertImages.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwConvertImages_DoWork);
            this.bwConvertImages.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwConvertImages_ProgressChanged);
            this.bwConvertImages.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwConvertImages_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.folderContentUserControl);
            this.panel1.Controls.Add(this.btnConvert);
            this.panel1.Controls.Add(this.lblDescription);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // folderContentUserControl
            // 
            this.folderContentUserControl.ImagesAmount = 0;
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // btnConvert
            // 
            resources.ApplyResources(this.btnConvert, "btnConvert");
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // lblDescription
            // 
            resources.ApplyResources(this.lblDescription, "lblDescription");
            this.lblDescription.Name = "lblDescription";
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // progressBar
            // 
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // ConvertHeicPhotos
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar);
            this.Icon = global::PiribiPhotoTools.Properties.Resources.photorenamer;
            this.Name = "ConvertHeicPhotos";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Custom.LabeledProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bwConvertImages;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnConvert;
        private Shared.FolderContentUserControl folderContentUserControl;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
        private System.Windows.Forms.Label lblDescription;
    }
}

