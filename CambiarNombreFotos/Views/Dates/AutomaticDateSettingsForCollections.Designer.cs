﻿namespace PiribiPhotoTools.Views.Dates
{
    partial class AutomaticDateSettingsForCollections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutomaticDateSettingsForCollections));
            this.label4 = new System.Windows.Forms.Label();
            this.destinationPictureBox = new System.Windows.Forms.PictureBox();
            this.btnDestNext = new System.Windows.Forms.Button();
            this.btnDestPrev = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.originPictureBox = new System.Windows.Forms.PictureBox();
            this.btnOriginNext = new System.Windows.Forms.Button();
            this.btnOriginPrev = new System.Windows.Forms.Button();
            this.btnBrowseDestination = new System.Windows.Forms.Button();
            this.btnBrowseOrigin = new System.Windows.Forms.Button();
            this.lblDestinationPath = new System.Windows.Forms.Label();
            this.lblOriginPath = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnChangeDates = new System.Windows.Forms.Button();
            this.bwAdjustDates = new System.ComponentModel.BackgroundWorker();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.lblOriginTakenDate = new System.Windows.Forms.Label();
            this.lblDestinyTakenDate = new System.Windows.Forms.Label();
            this.folderContentUserControlOrigin = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderContentUserControlDestination = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.destinationPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.originPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // destinationPictureBox
            // 
            resources.ApplyResources(this.destinationPictureBox, "destinationPictureBox");
            this.destinationPictureBox.Name = "destinationPictureBox";
            this.destinationPictureBox.TabStop = false;
            this.destinationPictureBox.DoubleClick += new System.EventHandler(this.destinationPictureBox_DoubleClick);
            this.destinationPictureBox.MouseLeave += new System.EventHandler(this.destinationPictureBox_MouseLeave);
            this.destinationPictureBox.MouseHover += new System.EventHandler(this.destinationPictureBox_MouseHover);
            // 
            // btnDestNext
            // 
            resources.ApplyResources(this.btnDestNext, "btnDestNext");
            this.btnDestNext.Name = "btnDestNext";
            this.btnDestNext.UseVisualStyleBackColor = true;
            this.btnDestNext.Click += new System.EventHandler(this.btnDestNext_Click);
            // 
            // btnDestPrev
            // 
            resources.ApplyResources(this.btnDestPrev, "btnDestPrev");
            this.btnDestPrev.Name = "btnDestPrev";
            this.btnDestPrev.UseVisualStyleBackColor = true;
            this.btnDestPrev.Click += new System.EventHandler(this.btnDestPrev_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // originPictureBox
            // 
            resources.ApplyResources(this.originPictureBox, "originPictureBox");
            this.originPictureBox.Name = "originPictureBox";
            this.originPictureBox.TabStop = false;
            this.originPictureBox.DoubleClick += new System.EventHandler(this.originPictureBox_DoubleClick);
            this.originPictureBox.MouseLeave += new System.EventHandler(this.originPictureBox_MouseLeave);
            this.originPictureBox.MouseHover += new System.EventHandler(this.originPictureBox_MouseHover);
            // 
            // btnOriginNext
            // 
            resources.ApplyResources(this.btnOriginNext, "btnOriginNext");
            this.btnOriginNext.Name = "btnOriginNext";
            this.btnOriginNext.UseVisualStyleBackColor = true;
            this.btnOriginNext.Click += new System.EventHandler(this.btnOriginNext_Click);
            // 
            // btnOriginPrev
            // 
            resources.ApplyResources(this.btnOriginPrev, "btnOriginPrev");
            this.btnOriginPrev.Name = "btnOriginPrev";
            this.btnOriginPrev.UseVisualStyleBackColor = true;
            this.btnOriginPrev.Click += new System.EventHandler(this.btnOriginPrev_Click);
            // 
            // btnBrowseDestination
            // 
            resources.ApplyResources(this.btnBrowseDestination, "btnBrowseDestination");
            this.btnBrowseDestination.Name = "btnBrowseDestination";
            this.btnBrowseDestination.UseVisualStyleBackColor = true;
            this.btnBrowseDestination.Click += new System.EventHandler(this.btnBrowseDestination_Click);
            // 
            // btnBrowseOrigin
            // 
            resources.ApplyResources(this.btnBrowseOrigin, "btnBrowseOrigin");
            this.btnBrowseOrigin.Name = "btnBrowseOrigin";
            this.btnBrowseOrigin.UseVisualStyleBackColor = true;
            this.btnBrowseOrigin.Click += new System.EventHandler(this.btnBrowseOrigin_Click);
            // 
            // lblDestinationPath
            // 
            resources.ApplyResources(this.lblDestinationPath, "lblDestinationPath");
            this.lblDestinationPath.Name = "lblDestinationPath";
            // 
            // lblOriginPath
            // 
            resources.ApplyResources(this.lblOriginPath, "lblOriginPath");
            this.lblOriginPath.Name = "lblOriginPath";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // btnChangeDates
            // 
            resources.ApplyResources(this.btnChangeDates, "btnChangeDates");
            this.btnChangeDates.Name = "btnChangeDates";
            this.btnChangeDates.UseVisualStyleBackColor = true;
            this.btnChangeDates.Click += new System.EventHandler(this.btnChangeDates_Click);
            // 
            // bwAdjustDates
            // 
            this.bwAdjustDates.WorkerReportsProgress = true;
            this.bwAdjustDates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAdjustDates_DoWork);
            this.bwAdjustDates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwAdjustDates_ProgressChanged);
            this.bwAdjustDates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAdjustDates_RunWorkerCompleted);
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            this.progressBar.Name = "progressBar";
            // 
            // lblOriginTakenDate
            // 
            resources.ApplyResources(this.lblOriginTakenDate, "lblOriginTakenDate");
            this.lblOriginTakenDate.Name = "lblOriginTakenDate";
            // 
            // lblDestinyTakenDate
            // 
            resources.ApplyResources(this.lblDestinyTakenDate, "lblDestinyTakenDate");
            this.lblDestinyTakenDate.Name = "lblDestinyTakenDate";
            // 
            // folderContentUserControlOrigin
            // 
            resources.ApplyResources(this.folderContentUserControlOrigin, "folderContentUserControlOrigin");
            this.folderContentUserControlOrigin.ImagesAmount = 0;
            this.folderContentUserControlOrigin.Name = "folderContentUserControlOrigin";
            this.folderContentUserControlOrigin.VideosAmount = 0;
            // 
            // folderContentUserControlDestination
            // 
            resources.ApplyResources(this.folderContentUserControlDestination, "folderContentUserControlDestination");
            this.folderContentUserControlDestination.ImagesAmount = 0;
            this.folderContentUserControlDestination.Name = "folderContentUserControlDestination";
            this.folderContentUserControlDestination.VideosAmount = 0;
            // 
            // folderBrowserDialog
            // 
            resources.ApplyResources(this.folderBrowserDialog, "folderBrowserDialog");
            // 
            // AutomaticDateSettingsForCollections
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderContentUserControlDestination);
            this.Controls.Add(this.folderContentUserControlOrigin);
            this.Controls.Add(this.lblDestinyTakenDate);
            this.Controls.Add(this.lblOriginTakenDate);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnChangeDates);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.destinationPictureBox);
            this.Controls.Add(this.btnDestNext);
            this.Controls.Add(this.btnDestPrev);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.originPictureBox);
            this.Controls.Add(this.btnOriginNext);
            this.Controls.Add(this.btnOriginPrev);
            this.Controls.Add(this.btnBrowseDestination);
            this.Controls.Add(this.btnBrowseOrigin);
            this.Controls.Add(this.lblDestinationPath);
            this.Controls.Add(this.lblOriginPath);
            this.Name = "AutomaticDateSettingsForCollections";
            ((System.ComponentModel.ISupportInitialize)(this.destinationPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.originPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox destinationPictureBox;
        private System.Windows.Forms.Button btnDestNext;
        private System.Windows.Forms.Button btnDestPrev;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox originPictureBox;
        private System.Windows.Forms.Button btnOriginNext;
        private System.Windows.Forms.Button btnOriginPrev;
        private System.Windows.Forms.Button btnBrowseDestination;
        private System.Windows.Forms.Button btnBrowseOrigin;
        private System.Windows.Forms.Label lblDestinationPath;
        private System.Windows.Forms.Label lblOriginPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnChangeDates;
        private System.ComponentModel.BackgroundWorker bwAdjustDates;
        private Custom.LabeledProgressBar progressBar;
        private System.Windows.Forms.Label lblOriginTakenDate;
        private System.Windows.Forms.Label lblDestinyTakenDate;
        private Shared.FolderContentUserControl folderContentUserControlOrigin;
        private Shared.FolderContentUserControl folderContentUserControlDestination;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}