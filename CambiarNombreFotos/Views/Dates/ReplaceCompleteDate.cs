﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Dates
{
    public partial class ReplaceCompleteDate : LocalizableForm, IPhotoToolsForm
    {
        public ReplaceCompleteDate()
        {
            InitializeComponent();

            ResetDateValues();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        #region Progress Bar Adjust Dates

        private void bwAdjustDates_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = FileHelper.GetMultimediaFiles(folderBrowserUserControl.SelectedFolder);
            var totalFiles = files.Count();
            var i = 1;
            foreach (var filepath in files)
            {
                if (chkTakenDate.Checked)
                {
                    // Change date picture taken.
                    MetadataHelper.ChangeDateInMetadata(filepath, datePicker.Value);
                }

                // Change creation datetime.
                if (chkCreationDate.Checked)
                {
                    File.SetCreationTime(filepath, datePicker.Value);
                }

                // Change modification datetime.
                if (chkModificationDate.Checked)
                {
                    File.SetLastWriteTime(filepath, datePicker.Value);
                }

                var progressValue = (i * 100 / totalFiles);
                bwAdjustDates.ReportProgress(progressValue);
                i++;
            }
        }

        private void bwAdjustDates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwAdjustDates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwAdjustDates_RunWorkerCompleted_AdjustmentFinished_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwAdjustDates_RunWorkerCompleted_AdjustmentFinished_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        #region Inits

        private void ResetDateValues()
        {
            // Set format and default value in datepicker.
            datePicker.Format = DateTimePickerFormat.Custom;
            datePicker.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            datePicker.Value = DateTime.Now;
        }

        #endregion

        #region Ajustar Fechas

        private void btnAdjustDate_Click(object sender, EventArgs e)
        {
            // Error directory is empty.
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("btnAdjustDate_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnAdjustDate_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!chkTakenDate.Checked && !chkCreationDate.Checked && !chkModificationDate.Checked)
            {
                // Needs to select at least one target.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bwAdjustDates.RunWorkerAsync();
            }
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
