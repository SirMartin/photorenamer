﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using NLog;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Dates
{
    public partial class CopyDates : LocalizableForm, IPhotoToolsForm
    {
        public CopyDates()
        {
            InitializeComponent();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        private void btnCopyDates_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!rbnOriginTakenDate.Checked && !rbnOriginModificationDate.Checked && !rbnOriginCreationDate.Checked)
            {
                // Needs to select at least one target.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_OriginDateNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_OriginDateNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!chkTargetTakenDate.Checked && !chkTargetCreationDate.Checked && !chkTargetModificationDate.Checked)
            {
                // Needs to select at least one target.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bwCopyDates.RunWorkerAsync();
            }
        }

        #region Origin Radio Buttons

        private void CleanCheckboxes()
        {
            chkTargetCreationDate.Checked = false;
            chkTargetModificationDate.Checked = false;
            chkTargetTakenDate.Checked = false;
        }

        private void rbnOriginCreationDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbnOriginCreationDate.Checked)
            {
                // Disable and enabled destination radio buttons.
                chkTargetCreationDate.Enabled = false;
                chkTargetModificationDate.Enabled = true;
                chkTargetTakenDate.Enabled = true;

                CleanCheckboxes();
            }
        }

        private void rbnOriginModificationDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbnOriginModificationDate.Checked)
            {
                // Disable and enabled destination radio buttons.
                chkTargetCreationDate.Enabled = true;
                chkTargetModificationDate.Enabled = false;
                chkTargetTakenDate.Enabled = true;

                CleanCheckboxes();
            }
        }

        private void rbnOriginTakenDate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbnOriginTakenDate.Checked)
            {
                // Disable and enabled destination radio buttons.
                chkTargetCreationDate.Enabled = true;
                chkTargetModificationDate.Enabled = true;
                chkTargetTakenDate.Enabled = false;

                CleanCheckboxes();
            }
        }

        #endregion

        #region Progress Bar Copy Dates

        private DateTime? GetSelectedDateTime(string filepath)
        {
            DateTime? targetDate = null;
            if (rbnOriginTakenDate.Checked)
            {
                var takenDate = MetadataHelper.GetPictureTakenDateFromMetadata(filepath);
                if (takenDate.HasValue)
                    targetDate = takenDate.Value;
            }
            else if (rbnOriginCreationDate.Checked)
            {
                targetDate = File.GetCreationTime(filepath);
            }
            else if (rbnOriginModificationDate.Checked)
            {
                targetDate = File.GetLastWriteTime(filepath);
            }

            return targetDate;
        }

        private void bwCopyDates_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = FileHelper.GetMultimediaFiles(folderBrowserUserControl.SelectedFolder);
            var totalFiles = files.Count();
            var i = 1;
            foreach (var filepath in files)
            {
                // Get the target date.
                var targetDate = GetSelectedDateTime(filepath);

                // Only copy the date, if we have a value.
                if (targetDate.HasValue)
                {
                    // Change date picture taken.
                    if (chkTargetTakenDate.Checked)
                    {
                        // Get modification date and also creation date to put it in the new file.
                        // Because we create a new file to change the taken date, after it.
                        var modificationDate = File.GetLastWriteTime(filepath);
                        var creationDate = File.GetCreationTime(filepath);

                        MetadataHelper.ChangeDateInMetadata(filepath, targetDate.Value);

                        // Update the dates after metadata changed.
                        File.SetCreationTime(filepath, creationDate);
                        File.SetLastWriteTime(filepath, modificationDate);
                    }

                    // Change creation datetime.
                    if (chkTargetCreationDate.Checked)
                    {
                        File.SetCreationTime(filepath, targetDate.Value);
                    }

                    // Change modification datetime.
                    if (chkTargetModificationDate.Checked || rbnOriginModificationDate.Checked)
                    {
                        File.SetLastWriteTime(filepath, targetDate.Value);
                    }
                }

                var progressValue = (i * 100 / totalFiles);
                bwCopyDates.ReportProgress(progressValue);
                i++;
            }
        }

        private void bwCopyDates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwCopyDates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwCopyDates_RunWorkerCompleted_FinishMessage_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwCopyDates_RunWorkerCompleted_FinishMessage_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
