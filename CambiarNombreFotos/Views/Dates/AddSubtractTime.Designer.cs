﻿namespace PiribiPhotoTools.Views.Dates
{
    partial class AddSubtractTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddSubtractTime));
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.bwAdjustDates = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkTakenDate = new System.Windows.Forms.CheckBox();
            this.chkCreationDate = new System.Windows.Forms.CheckBox();
            this.chkModificationDate = new System.Windows.Forms.CheckBox();
            this.btnAdjustDate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panAddRestarTiempo = new System.Windows.Forms.Panel();
            this.rbnSubtract = new System.Windows.Forms.RadioButton();
            this.rbnAdd = new System.Windows.Forms.RadioButton();
            this.txtSeconds = new System.Windows.Forms.TextBox();
            this.txtMinutes = new System.Windows.Forms.TextBox();
            this.txtHours = new System.Windows.Forms.TextBox();
            this.txtDays = new System.Windows.Forms.TextBox();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.lblMinutes = new System.Windows.Forms.Label();
            this.lblHours = new System.Windows.Forms.Label();
            this.lblDays = new System.Windows.Forms.Label();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panAddRestarTiempo.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // bwAdjustDates
            // 
            this.bwAdjustDates.WorkerReportsProgress = true;
            this.bwAdjustDates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAdjustDates_DoWork);
            this.bwAdjustDates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwAdjustDates_ProgressChanged);
            this.bwAdjustDates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAdjustDates_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkTakenDate);
            this.panel1.Controls.Add(this.chkCreationDate);
            this.panel1.Controls.Add(this.chkModificationDate);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkTakenDate
            // 
            resources.ApplyResources(this.chkTakenDate, "chkTakenDate");
            this.chkTakenDate.Name = "chkTakenDate";
            this.chkTakenDate.UseVisualStyleBackColor = true;
            // 
            // chkCreationDate
            // 
            resources.ApplyResources(this.chkCreationDate, "chkCreationDate");
            this.chkCreationDate.Name = "chkCreationDate";
            this.chkCreationDate.UseVisualStyleBackColor = true;
            // 
            // chkModificationDate
            // 
            resources.ApplyResources(this.chkModificationDate, "chkModificationDate");
            this.chkModificationDate.Name = "chkModificationDate";
            this.chkModificationDate.UseVisualStyleBackColor = true;
            // 
            // btnAdjustDate
            // 
            resources.ApplyResources(this.btnAdjustDate, "btnAdjustDate");
            this.btnAdjustDate.Name = "btnAdjustDate";
            this.btnAdjustDate.UseVisualStyleBackColor = true;
            this.btnAdjustDate.Click += new System.EventHandler(this.btnAdjustDate_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panAddRestarTiempo);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panAddRestarTiempo
            // 
            this.panAddRestarTiempo.Controls.Add(this.rbnSubtract);
            this.panAddRestarTiempo.Controls.Add(this.rbnAdd);
            this.panAddRestarTiempo.Controls.Add(this.txtSeconds);
            this.panAddRestarTiempo.Controls.Add(this.txtMinutes);
            this.panAddRestarTiempo.Controls.Add(this.txtHours);
            this.panAddRestarTiempo.Controls.Add(this.txtDays);
            this.panAddRestarTiempo.Controls.Add(this.lblSeconds);
            this.panAddRestarTiempo.Controls.Add(this.lblMinutes);
            this.panAddRestarTiempo.Controls.Add(this.lblHours);
            this.panAddRestarTiempo.Controls.Add(this.lblDays);
            resources.ApplyResources(this.panAddRestarTiempo, "panAddRestarTiempo");
            this.panAddRestarTiempo.Name = "panAddRestarTiempo";
            // 
            // rbnSubtract
            // 
            resources.ApplyResources(this.rbnSubtract, "rbnSubtract");
            this.rbnSubtract.Name = "rbnSubtract";
            this.rbnSubtract.TabStop = true;
            this.rbnSubtract.UseVisualStyleBackColor = true;
            // 
            // rbnAdd
            // 
            resources.ApplyResources(this.rbnAdd, "rbnAdd");
            this.rbnAdd.Checked = true;
            this.rbnAdd.Name = "rbnAdd";
            this.rbnAdd.TabStop = true;
            this.rbnAdd.UseVisualStyleBackColor = true;
            // 
            // txtSeconds
            // 
            resources.ApplyResources(this.txtSeconds, "txtSeconds");
            this.txtSeconds.Name = "txtSeconds";
            this.txtSeconds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumericInTextBox_KeyPress);
            // 
            // txtMinutes
            // 
            resources.ApplyResources(this.txtMinutes, "txtMinutes");
            this.txtMinutes.Name = "txtMinutes";
            this.txtMinutes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumericInTextBox_KeyPress);
            // 
            // txtHours
            // 
            resources.ApplyResources(this.txtHours, "txtHours");
            this.txtHours.Name = "txtHours";
            this.txtHours.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumericInTextBox_KeyPress);
            // 
            // txtDays
            // 
            resources.ApplyResources(this.txtDays, "txtDays");
            this.txtDays.Name = "txtDays";
            this.txtDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumericInTextBox_KeyPress);
            // 
            // lblSeconds
            // 
            resources.ApplyResources(this.lblSeconds, "lblSeconds");
            this.lblSeconds.Name = "lblSeconds";
            // 
            // lblMinutes
            // 
            resources.ApplyResources(this.lblMinutes, "lblMinutes");
            this.lblMinutes.Name = "lblMinutes";
            // 
            // lblHours
            // 
            resources.ApplyResources(this.lblHours, "lblHours");
            this.lblHours.Name = "lblHours";
            // 
            // lblDays
            // 
            resources.ApplyResources(this.lblDays, "lblDays");
            this.lblDays.Name = "lblDays";
            // 
            // folderContentUserControl
            // 
            this.folderContentUserControl.ImagesAmount = 0;
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // AddSubtractTime
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.folderContentUserControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAdjustDate);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.progressBar);
            this.Icon = global::PiribiPhotoTools.Properties.Resources.photorenamer;
            this.Name = "AddSubtractTime";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panAddRestarTiempo.ResumeLayout(false);
            this.panAddRestarTiempo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Custom.LabeledProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bwAdjustDates;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkTakenDate;
        private System.Windows.Forms.CheckBox chkCreationDate;
        private System.Windows.Forms.CheckBox chkModificationDate;
        private System.Windows.Forms.Button btnAdjustDate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panAddRestarTiempo;
        private System.Windows.Forms.RadioButton rbnSubtract;
        private System.Windows.Forms.RadioButton rbnAdd;
        private System.Windows.Forms.TextBox txtSeconds;
        private System.Windows.Forms.TextBox txtMinutes;
        private System.Windows.Forms.TextBox txtHours;
        private System.Windows.Forms.TextBox txtDays;
        private System.Windows.Forms.Label lblSeconds;
        private System.Windows.Forms.Label lblMinutes;
        private System.Windows.Forms.Label lblHours;
        private System.Windows.Forms.Label lblDays;
        private Shared.FolderContentUserControl folderContentUserControl;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
    }
}

