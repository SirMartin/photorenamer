﻿namespace PiribiPhotoTools.Views.Dates
{
    partial class CopyDatesFromName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bwCopyDates = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFormat = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCopyDates = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panModificarFecha = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.chkTargetTakenDate = new System.Windows.Forms.CheckBox();
            this.chkTargetCreationDate = new System.Windows.Forms.CheckBox();
            this.chkTargetModificationDate = new System.Windows.Forms.CheckBox();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panModificarFecha.SuspendLayout();
            this.SuspendLayout();
            // 
            // bwCopyDates
            // 
            this.bwCopyDates.WorkerReportsProgress = true;
            this.bwCopyDates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCopyDates_DoWork);
            this.bwCopyDates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwCopyDates_ProgressChanged);
            this.bwCopyDates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwCopyDates_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtFormat);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(25, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(408, 43);
            this.panel1.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(208, 39);
            this.label3.TabIndex = 5;
            this.label3.Text = "dd = days, MM = month, yy = year 2 digits, \r\nyyyy = year 4 digits, hh = hour, \r\nm" +
    "m = minutes, ss = seconds";
            // 
            // txtFormat
            // 
            this.txtFormat.Location = new System.Drawing.Point(9, 20);
            this.txtFormat.Name = "txtFormat";
            this.txtFormat.Size = new System.Drawing.Size(145, 20);
            this.txtFormat.TabIndex = 4;
            this.txtFormat.Text = "ddMMyyyyhhmmss";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Format:";
            // 
            // btnCopyDates
            // 
            this.btnCopyDates.Location = new System.Drawing.Point(377, 199);
            this.btnCopyDates.Name = "btnCopyDates";
            this.btnCopyDates.Size = new System.Drawing.Size(75, 23);
            this.btnCopyDates.TabIndex = 21;
            this.btnCopyDates.Text = "Copy";
            this.btnCopyDates.UseVisualStyleBackColor = true;
            this.btnCopyDates.Click += new System.EventHandler(this.btnCopyDates_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panModificarFecha);
            this.panel2.Location = new System.Drawing.Point(25, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(408, 99);
            this.panel2.TabIndex = 23;
            // 
            // panModificarFecha
            // 
            this.panModificarFecha.BackColor = System.Drawing.Color.Transparent;
            this.panModificarFecha.Controls.Add(this.label2);
            this.panModificarFecha.Controls.Add(this.chkTargetTakenDate);
            this.panModificarFecha.Controls.Add(this.chkTargetCreationDate);
            this.panModificarFecha.Controls.Add(this.chkTargetModificationDate);
            this.panModificarFecha.Location = new System.Drawing.Point(3, 0);
            this.panModificarFecha.Name = "panModificarFecha";
            this.panModificarFecha.Size = new System.Drawing.Size(402, 78);
            this.panModificarFecha.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Copiar to...";
            // 
            // chkTargetTakenDate
            // 
            this.chkTargetTakenDate.AutoSize = true;
            this.chkTargetTakenDate.Location = new System.Drawing.Point(229, 28);
            this.chkTargetTakenDate.Name = "chkTargetTakenDate";
            this.chkTargetTakenDate.Size = new System.Drawing.Size(83, 17);
            this.chkTargetTakenDate.TabIndex = 22;
            this.chkTargetTakenDate.Text = "Date Taken";
            this.chkTargetTakenDate.UseVisualStyleBackColor = true;
            // 
            // chkTargetCreationDate
            // 
            this.chkTargetCreationDate.AutoSize = true;
            this.chkTargetCreationDate.Location = new System.Drawing.Point(122, 28);
            this.chkTargetCreationDate.Name = "chkTargetCreationDate";
            this.chkTargetCreationDate.Size = new System.Drawing.Size(89, 17);
            this.chkTargetCreationDate.TabIndex = 21;
            this.chkTargetCreationDate.Text = "Date Created";
            this.chkTargetCreationDate.UseVisualStyleBackColor = true;
            // 
            // chkTargetModificationDate
            // 
            this.chkTargetModificationDate.AutoSize = true;
            this.chkTargetModificationDate.Location = new System.Drawing.Point(3, 28);
            this.chkTargetModificationDate.Name = "chkTargetModificationDate";
            this.chkTargetModificationDate.Size = new System.Drawing.Size(92, 17);
            this.chkTargetModificationDate.TabIndex = 20;
            this.chkTargetModificationDate.Text = "Date Modified";
            this.chkTargetModificationDate.UseVisualStyleBackColor = true;
            // 
            // progressBar
            // 
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            this.progressBar.Location = new System.Drawing.Point(12, 358);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(495, 23);
            this.progressBar.TabIndex = 24;
            // 
            // folderContentUserControl
            // 
            this.folderContentUserControl.ImagesAmount = 0;
            this.folderContentUserControl.Location = new System.Drawing.Point(25, 214);
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.Size = new System.Drawing.Size(154, 83);
            this.folderContentUserControl.TabIndex = 25;
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // folderBrowserUserControl
            // 
            this.folderBrowserUserControl.Location = new System.Drawing.Point(18, 8);
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            this.folderBrowserUserControl.Size = new System.Drawing.Size(434, 31);
            this.folderBrowserUserControl.TabIndex = 26;
            // 
            // CopyDatesFromName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(520, 393);
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.folderContentUserControl);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCopyDates);
            this.Controls.Add(this.panel2);
            this.Name = "CopyDatesFromName";
            this.Text = "Copy Dates";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panModificarFecha.ResumeLayout(false);
            this.panModificarFecha.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker bwCopyDates;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCopyDates;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panModificarFecha;
        private Custom.LabeledProgressBar progressBar;
        private System.Windows.Forms.CheckBox chkTargetTakenDate;
        private System.Windows.Forms.CheckBox chkTargetCreationDate;
        private System.Windows.Forms.CheckBox chkTargetModificationDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Shared.FolderContentUserControl folderContentUserControl;
        private System.Windows.Forms.TextBox txtFormat;
        private System.Windows.Forms.Label label3;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
    }
}