﻿namespace PiribiPhotoTools.Views.Dates
{
    partial class CopyDates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyDates));
            this.bwCopyDates = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.rbnOriginTakenDate = new System.Windows.Forms.RadioButton();
            this.rbnOriginModificationDate = new System.Windows.Forms.RadioButton();
            this.rbnOriginCreationDate = new System.Windows.Forms.RadioButton();
            this.btnCopyDates = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panModificarFecha = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.chkTargetTakenDate = new System.Windows.Forms.CheckBox();
            this.chkTargetCreationDate = new System.Windows.Forms.CheckBox();
            this.chkTargetModificationDate = new System.Windows.Forms.CheckBox();
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panModificarFecha.SuspendLayout();
            this.SuspendLayout();
            // 
            // bwCopyDates
            // 
            this.bwCopyDates.WorkerReportsProgress = true;
            this.bwCopyDates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwCopyDates_DoWork);
            this.bwCopyDates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwCopyDates_ProgressChanged);
            this.bwCopyDates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwCopyDates_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.rbnOriginTakenDate);
            this.panel1.Controls.Add(this.rbnOriginModificationDate);
            this.panel1.Controls.Add(this.rbnOriginCreationDate);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // rbnOriginTakenDate
            // 
            resources.ApplyResources(this.rbnOriginTakenDate, "rbnOriginTakenDate");
            this.rbnOriginTakenDate.Name = "rbnOriginTakenDate";
            this.rbnOriginTakenDate.TabStop = true;
            this.rbnOriginTakenDate.UseVisualStyleBackColor = true;
            this.rbnOriginTakenDate.CheckedChanged += new System.EventHandler(this.rbnOriginTakenDate_CheckedChanged);
            // 
            // rbnOriginModificationDate
            // 
            resources.ApplyResources(this.rbnOriginModificationDate, "rbnOriginModificationDate");
            this.rbnOriginModificationDate.Name = "rbnOriginModificationDate";
            this.rbnOriginModificationDate.TabStop = true;
            this.rbnOriginModificationDate.UseVisualStyleBackColor = true;
            this.rbnOriginModificationDate.CheckedChanged += new System.EventHandler(this.rbnOriginModificationDate_CheckedChanged);
            // 
            // rbnOriginCreationDate
            // 
            resources.ApplyResources(this.rbnOriginCreationDate, "rbnOriginCreationDate");
            this.rbnOriginCreationDate.Name = "rbnOriginCreationDate";
            this.rbnOriginCreationDate.TabStop = true;
            this.rbnOriginCreationDate.UseVisualStyleBackColor = true;
            this.rbnOriginCreationDate.CheckedChanged += new System.EventHandler(this.rbnOriginCreationDate_CheckedChanged);
            // 
            // btnCopyDates
            // 
            resources.ApplyResources(this.btnCopyDates, "btnCopyDates");
            this.btnCopyDates.Name = "btnCopyDates";
            this.btnCopyDates.UseVisualStyleBackColor = true;
            this.btnCopyDates.Click += new System.EventHandler(this.btnCopyDates_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panModificarFecha);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panModificarFecha
            // 
            this.panModificarFecha.BackColor = System.Drawing.Color.Transparent;
            this.panModificarFecha.Controls.Add(this.label2);
            this.panModificarFecha.Controls.Add(this.chkTargetTakenDate);
            this.panModificarFecha.Controls.Add(this.chkTargetCreationDate);
            this.panModificarFecha.Controls.Add(this.chkTargetModificationDate);
            resources.ApplyResources(this.panModificarFecha, "panModificarFecha");
            this.panModificarFecha.Name = "panModificarFecha";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // chkTargetTakenDate
            // 
            resources.ApplyResources(this.chkTargetTakenDate, "chkTargetTakenDate");
            this.chkTargetTakenDate.Name = "chkTargetTakenDate";
            this.chkTargetTakenDate.UseVisualStyleBackColor = true;
            // 
            // chkTargetCreationDate
            // 
            resources.ApplyResources(this.chkTargetCreationDate, "chkTargetCreationDate");
            this.chkTargetCreationDate.Name = "chkTargetCreationDate";
            this.chkTargetCreationDate.UseVisualStyleBackColor = true;
            // 
            // chkTargetModificationDate
            // 
            resources.ApplyResources(this.chkTargetModificationDate, "chkTargetModificationDate");
            this.chkTargetModificationDate.Name = "chkTargetModificationDate";
            this.chkTargetModificationDate.UseVisualStyleBackColor = true;
            // 
            // progressBar
            // 
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // folderContentUserControl
            // 
            this.folderContentUserControl.ImagesAmount = 0;
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // CopyDates
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.folderContentUserControl);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCopyDates);
            this.Controls.Add(this.panel2);
            this.Name = "CopyDates";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panModificarFecha.ResumeLayout(false);
            this.panModificarFecha.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker bwCopyDates;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCopyDates;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panModificarFecha;
        private Custom.LabeledProgressBar progressBar;
        private System.Windows.Forms.RadioButton rbnOriginTakenDate;
        private System.Windows.Forms.RadioButton rbnOriginModificationDate;
        private System.Windows.Forms.RadioButton rbnOriginCreationDate;
        private System.Windows.Forms.CheckBox chkTargetTakenDate;
        private System.Windows.Forms.CheckBox chkTargetCreationDate;
        private System.Windows.Forms.CheckBox chkTargetModificationDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Shared.FolderContentUserControl folderContentUserControl;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
    }
}