﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using NLog;
using Ookii.Dialogs.WinForms;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Dates
{
    public partial class AutomaticDateSettingsForCollections : LocalizableForm, IPhotoToolsForm
    {
        public AutomaticDateSettingsForCollections()
        {
            InitializeComponent();

            CleanLabels();
        }

        private void CleanLabels()
        {
            lblOriginTakenDate.Text = string.Empty;
            lblDestinyTakenDate.Text = string.Empty;
            lblOriginPath.Text = string.Empty;
            lblDestinationPath.Text = string.Empty;
        }

        #region Properties

        public string LabelTakeDateOrigin
        {
            set { lblOriginTakenDate.Text = value; }
        }

        public string LabelTakeDateDestiny
        {
            set { lblDestinyTakenDate.Text = value; }
        }

        private string _selectedFolderOrigin;
        public string SelectedFolderOrigin
        {
            get { return _selectedFolderOrigin; }
            set
            {
                _selectedFolderOrigin = value;
                lblOriginPath.Text = _selectedFolderOrigin;
            }
        }

        private string _selectedFolderDestination;
        public string SelectedFolderDestination
        {
            get { return _selectedFolderDestination; }
            set
            {
                _selectedFolderDestination = value;
                lblDestinationPath.Text = _selectedFolderDestination;
            }
        }

        public string[] OriginPictureList { get; set; }

        public int SelectedOriginFileInList { get; set; }

        public string[] DestinationPictureList { get; set; }

        public int SelectedDestinationFileInList { get; set; }

        #endregion

        #region Browse buttons

        private void btnBrowseOrigin_Click(object sender, EventArgs e)
        {
            // New folderbrowser system.
            if (VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                var vistaFolderBrowserDialog = new VistaFolderBrowserDialog();
                var result = vistaFolderBrowserDialog.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    // the code here will be executed if the user presses Open in
                    // the dialog.
                    SelectedFolderOrigin = vistaFolderBrowserDialog.SelectedPath;
                    lblOriginPath.Text = SelectedFolderOrigin;

                    LoadOriginPicture();

                    // Load number of files labels.
                    folderContentUserControlOrigin.CalculateContent(SelectedFolderOrigin);
                }
            }
            else
            {
                folderBrowserDialog.ShowNewFolderButton = false;
                folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

                DialogResult result = folderBrowserDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    // the code here will be executed if the user presses Open in
                    // the dialog.
                    SelectedFolderOrigin = folderBrowserDialog.SelectedPath;
                    lblOriginPath.Text = SelectedFolderOrigin;

                    LoadOriginPicture();

                    // Load number of files labels.
                    folderContentUserControlOrigin.CalculateContent(SelectedFolderOrigin);
                }
            }
        }

        private void btnBrowseDestination_Click(object sender, EventArgs e)
        {
            // New folderbrowser system.
            if (VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                var vistaFolderBrowserDialog = new VistaFolderBrowserDialog();
                var result = vistaFolderBrowserDialog.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    // the code here will be executed if the user presses Open in
                    // the dialog.
                    SelectedFolderDestination = vistaFolderBrowserDialog.SelectedPath;
                    lblDestinationPath.Text = SelectedFolderDestination;

                    LoadDestinationPicture();

                    // Load number of files labels.
                    folderContentUserControlDestination.CalculateContent(SelectedFolderDestination);
                }
            }
            else
            {
                folderBrowserDialog.ShowNewFolderButton = false;
                folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

                DialogResult result = folderBrowserDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    // the code here will be executed if the user presses Open in
                    // the dialog.
                    SelectedFolderDestination = folderBrowserDialog.SelectedPath;
                    lblDestinationPath.Text = SelectedFolderDestination;

                    LoadDestinationPicture();

                    // Load number of files labels.
                    folderContentUserControlDestination.CalculateContent(SelectedFolderDestination);
                }
            }
        }

        #endregion

        #region Load Pictures Methods

        private void LoadOriginPicture()
        {
            OriginPictureList = FileHelper.GetImageFilesFrom(SelectedFolderOrigin, false).ToArray();

            // Set first picture in the pictureBox.
            SelectedOriginFileInList = 0;
            originPictureBox.Image = Image.FromFile(OriginPictureList[SelectedOriginFileInList]);
            originPictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            // Set taken date in label.
            var datetime = MetadataHelper.GetPictureTakenDateFromMetadata(OriginPictureList[SelectedOriginFileInList]);
            LabelTakeDateOrigin = datetime.HasValue ? datetime.Value.ToString() : string.Empty;
        }

        private void LoadDestinationPicture()
        {
            DestinationPictureList = FileHelper.GetImageFilesFrom(SelectedFolderDestination, false).ToArray();

            // Set first picture in the pictureBox.
            SelectedDestinationFileInList = 0;
            destinationPictureBox.Image = Image.FromFile(DestinationPictureList[SelectedDestinationFileInList]);
            destinationPictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            // Set taken date in label.
            var datetime = MetadataHelper.GetPictureTakenDateFromMetadata(DestinationPictureList[SelectedDestinationFileInList]);
            LabelTakeDateDestiny = datetime.HasValue ? datetime.Value.ToString() : string.Empty;
        }

        #endregion

        #region Next and previous buttons in picture boxes.

        private void btnOriginPrev_Click(object sender, EventArgs e)
        {
            SelectedOriginFileInList--;
            if (SelectedOriginFileInList < 0)
                SelectedOriginFileInList = OriginPictureList.Count() - 1;

            originPictureBox.Image = Image.FromFile(OriginPictureList[SelectedOriginFileInList]);
            originPictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            // Set taken date in label.
            var datetime = MetadataHelper.GetPictureTakenDateFromMetadata(OriginPictureList[SelectedOriginFileInList]);
            LabelTakeDateOrigin = datetime.HasValue ? datetime.Value.ToString() : string.Empty;
        }

        private void btnOriginNext_Click(object sender, EventArgs e)
        {
            SelectedOriginFileInList++;
            if (SelectedOriginFileInList >= OriginPictureList.Count())
                SelectedOriginFileInList = 0;

            originPictureBox.Image = Image.FromFile(OriginPictureList[SelectedOriginFileInList]);
            originPictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            // Set taken date in label.
            var datetime = MetadataHelper.GetPictureTakenDateFromMetadata(OriginPictureList[SelectedOriginFileInList]);
            LabelTakeDateOrigin = datetime.HasValue ? datetime.Value.ToString() : string.Empty;
        }

        private void btnDestPrev_Click(object sender, EventArgs e)
        {
            SelectedDestinationFileInList--;
            if (SelectedDestinationFileInList < 0)
                SelectedDestinationFileInList = DestinationPictureList.Count() - 1;

            destinationPictureBox.Image = Image.FromFile(DestinationPictureList[SelectedDestinationFileInList]);
            destinationPictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            // Set taken date in label.
            var datetime = MetadataHelper.GetPictureTakenDateFromMetadata(DestinationPictureList[SelectedDestinationFileInList]);
            LabelTakeDateDestiny = datetime.HasValue ? datetime.Value.ToString() : string.Empty;
        }

        private void btnDestNext_Click(object sender, EventArgs e)
        {
            SelectedDestinationFileInList++;
            if (SelectedDestinationFileInList >= DestinationPictureList.Count())
                SelectedDestinationFileInList = 0;

            destinationPictureBox.Image = Image.FromFile(DestinationPictureList[SelectedDestinationFileInList]);
            destinationPictureBox.SizeMode = PictureBoxSizeMode.Zoom;

            // Set taken date in label.
            var datetime = MetadataHelper.GetPictureTakenDateFromMetadata(DestinationPictureList[SelectedDestinationFileInList]);
            LabelTakeDateDestiny = datetime.HasValue ? datetime.Value.ToString() : string.Empty;
        }

        #endregion

        #region Action Methods

        private void btnChangeDates_Click(object sender, EventArgs e)
        {
            // Do the work.
            bwAdjustDates.RunWorkerAsync();
        }

        #endregion

        #region Progress Bar Adjust Dates

        private void bwAdjustDates_DoWork(object sender, DoWorkEventArgs e)
        {
            #region Take photos and time

            // Get the two photos and the difference.
            var originFile = OriginPictureList[SelectedOriginFileInList];
            var destinyFile = DestinationPictureList[SelectedDestinationFileInList];

            var originDatetime = MetadataHelper.GetPictureTakenDateFromMetadata(originFile);
            if (!originDatetime.HasValue)
            {
                // Error directory is empty.
                MessageBox.Show("La fotografía de origen elegida no contiene ninguna fecha de creación.", "ERROR", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

            // Set in the label the taken date.
            LabelTakeDateOrigin = originDatetime.Value.ToString();

            var destinyDatetime = MetadataHelper.GetPictureTakenDateFromMetadata(destinyFile);
            if (!destinyDatetime.HasValue)
            {
                // Error directory is empty.
                MessageBox.Show("La fotografía de destino elegida no contiene ninguna fecha de creación.", "ERROR", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

            // Set in the label the taken date.
            LabelTakeDateDestiny = destinyDatetime.Value.ToString();

            // Get the difference of time, between photos.
            var timespanDifference = originDatetime.Value.Subtract(destinyDatetime.Value);

            #endregion

            var files = FileHelper.GetImageFilesFrom(SelectedFolderDestination, false);
            var totalFiles = files.Count();

            Logger.Info("Founded " + totalFiles + " files in the directory.");

            int i = 1;
            foreach (var filepath in files)
            {
                var progressValue = (i * 100 / totalFiles);

                if (!FileHelper.IsImageWithMetadataFile(filepath))
                {
                    // Report progress
                    bwAdjustDates.ReportProgress(progressValue);
                    i++;
                    continue;
                }

                try
                {
                    // Check if the picture to modify is the picture is selected in the destiny picture box.
                    bool isPictureBoxImage = false;
                    if (DestinationPictureList[SelectedDestinationFileInList] == filepath)
                    {
                        isPictureBoxImage = true;
                        // Remove temporaly from the picture box.
                        destinationPictureBox.Image = null;
                        System.Threading.Thread.Sleep(1000);
                    }

                    // Change date picture taken.
                    var datetimeMetadata = MetadataHelper.GetPictureTakenDateFromMetadata(filepath).Value;
                    datetimeMetadata = datetimeMetadata.Add(timespanDifference);
                    MetadataHelper.ChangePictureDateInMetadata(filepath, datetimeMetadata);

                    // Report progress
                    bwAdjustDates.ReportProgress(progressValue);
                    i++;

                    // Set again the image in the picture box. Only if its the selected image.
                    if (isPictureBoxImage)
                    {
                        destinationPictureBox.Image = Image.FromFile(DestinationPictureList[SelectedDestinationFileInList]);
                        destinationPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                    }
                }
                catch (Exception ex)
                {
                    // Report progress
                    bwAdjustDates.ReportProgress(progressValue);
                    i++;

                    Logger.Error("Error Converting Image {0}/{1}", i, totalFiles);
                    Logger.Error("Error Exception", ex);
                }
            }
        }

        private void bwAdjustDates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
            Logger.Info("Procesando imagenes automaticamente - {0}%", e.ProgressPercentage);
        }

        private void bwAdjustDates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("El ajuste de fechas ha finalizado.", "INFO", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            Logger.Info("Terminado de procesar imagenes.");

        }

        #endregion

        #region Zoom in picture box

        /// <summary>
        /// Show the Zoom form to check the images when make double click in the origin picture box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void originPictureBox_DoubleClick(object sender, EventArgs e)
        {
            ShowPicturesInZoom();
        }

        /// <summary>
        /// Show the Zoom form to check the images when make double click in the destination picture box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void destinationPictureBox_DoubleClick(object sender, EventArgs e)
        {
            ShowPicturesInZoom();
        }

        /// <summary>
        /// Method call to the zoom form with the selected images.
        /// </summary>
        private void ShowPicturesInZoom()
        {
            var originPicture = OriginPictureList != null && OriginPictureList.Any() ? OriginPictureList[SelectedOriginFileInList] : null;
            var destinationPicture = DestinationPictureList != null && DestinationPictureList.Any() ? DestinationPictureList[SelectedDestinationFileInList] : null;
            var zoomForm = new ZoomPictures(originPicture, destinationPicture);
            zoomForm.Show(this);
        }

        #endregion

        #region Picture box options

        private void destinationPictureBox_MouseHover(object sender, EventArgs e)
        {
            // Change cursor.
            Cursor = Cursors.Hand;
            // Tooltip
            var tt = new ToolTip();
            tt.SetToolTip(destinationPictureBox, "Doble click para zoom de la imagen.");
        }

        private void originPictureBox_MouseHover(object sender, EventArgs e)
        {
            // Change cursor.
            Cursor = Cursors.Hand;
            // Tooltip
            var tt = new ToolTip();
            tt.SetToolTip(originPictureBox, "Doble click para zoom de la imagen.");
        }

        private void destinationPictureBox_MouseLeave(object sender, EventArgs e)
        {
            // Change cursor again.
            Cursor = Cursors.Default;
        }

        private void originPictureBox_MouseLeave(object sender, EventArgs e)
        {
            // Change cursor again.
            Cursor = Cursors.Default;
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
