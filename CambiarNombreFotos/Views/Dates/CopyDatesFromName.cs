﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using NLog;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Dates
{
    public partial class CopyDatesFromName : LocalizableForm, IPhotoToolsForm
    {
        public CopyDatesFromName()
        {
            InitializeComponent();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }
        
        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }
        
        private void btnCopyDates_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!chkTargetTakenDate.Checked && !chkTargetCreationDate.Checked && !chkTargetModificationDate.Checked)
            {
                // Needs to select at least one target.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bwCopyDates.RunWorkerAsync();
            }
        }

        #region Progress Bar Copy Dates

        private DateTime? GetSelectedDateTime(string filepath)
        {
            var filename = Path.GetFileNameWithoutExtension(filepath);

            if (filename.Length == txtFormat.Text.Length)
            {
                // Needs to have the same length for parse it.
                var resultDate = ParseDateFromFilenameAndFormat(filename, txtFormat.Text);
                return resultDate;
            }

            return null;
        }

        #region Parse parts of the date.

        private DateTime? ParseDateFromFilenameAndFormat(string filename, string filenameFormat)
        {
            // Minimum of day, month and year is need, so if one of this 3 is null, we return a null.
            var day = ParseDay(filename, filenameFormat);
            var month = ParseMonth(filename, filenameFormat);
            var year = ParseYear(filename, filenameFormat);

            if (!day.HasValue || !month.HasValue || !year.HasValue)
            {
                // Return null, because almost one of the important three, it hasn't value.
                return null;
            }

            // Check hour, minutes and seconds.
            var hour = ParseHour(filename, filenameFormat);
            var minute = ParseMinute(filename, filenameFormat);
            var second = ParseSecond(filename, filenameFormat);

            // Create the date.
            var resultDate = new DateTime(year.Value, month.Value, day.Value, hour ?? 0, minute ?? 0, second ?? 0);
            return resultDate;
        }

        private int? ParsePartOfDay(string filename, string filenameFormat, string formatToFind)
        {
            var result = "";
            if (filenameFormat.IndexOf(formatToFind) == -1)
            {
                return null;
            }

            // Get the substring.
            result = filename.Substring(filenameFormat.IndexOf(formatToFind), formatToFind.Length);

            if (int.TryParse(result, out var parsedResult))
            {
                return parsedResult;
            }

            return null;
        }

        private int? ParseSecond(string filename, string filenameFormat)
        {
            // Check first with 2 (ss).
            var result = ParsePartOfDay(filename, filenameFormat, "ss") ?? ParsePartOfDay(filename, filenameFormat, "s");

            return result;
        }

        private int? ParseMinute(string filename, string filenameFormat)
        {
            // Check first with 2 (mm).
            var result = ParsePartOfDay(filename, filenameFormat, "mm") ?? ParsePartOfDay(filename, filenameFormat, "m");

            return result;
        }

        private int? ParseHour(string filename, string filenameFormat)
        {
            // Check first with 2 (hh).
            var result = ParsePartOfDay(filename, filenameFormat, "hh") ?? ParsePartOfDay(filename, filenameFormat, "h");

            return result;
        }

        private int? ParseDay(string filename, string filenameFormat)
        {
            // Check first with 2 (dd).
            var result = ParsePartOfDay(filename, filenameFormat, "dd") ?? ParsePartOfDay(filename, filenameFormat, "d");

            return result;
        }

        private int? ParseMonth(string filename, string filenameFormat)
        {
            // Check first with 2 (MM).
            var result = ParsePartOfDay(filename, filenameFormat, "MM") ?? ParsePartOfDay(filename, filenameFormat, "M");

            return result;
        }

        private int? ParseYear(string filename, string filenameFormat)
        {
            // Check first with 4 (YYYY).
            var result = ParsePartOfDay(filename, filenameFormat, "yyyy");
            if (!result.HasValue)
            {
                // Try only with 2 (YY).
                result = ParsePartOfDay(filename, filenameFormat, "yy");
                result = 2000 + result;
            }

            return result;
        }

        #endregion

        private void bwCopyDates_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = FileHelper.GetMultimediaFiles(folderBrowserUserControl.SelectedFolder);
            var totalFiles = files.Count();
            var i = 1;
            foreach (var filepath in files)
            {
                // Get the target date.
                var targetDate = GetSelectedDateTime(filepath);

                // Only copy the date, if we have a value.
                if (targetDate.HasValue)
                {
                    // Change date picture taken.
                    if (chkTargetTakenDate.Checked)
                    {
                        MetadataHelper.ChangeDateInMetadata(filepath, targetDate.Value);
                    }

                    // Change creation datetime.
                    if (chkTargetCreationDate.Checked)
                    {
                        File.SetCreationTime(filepath, targetDate.Value);
                    }

                    // Change modification datetime.
                    if (chkTargetModificationDate.Checked)
                    {
                        File.SetLastWriteTime(filepath, targetDate.Value);
                    }
                }

                var progressValue = (i * 100 / totalFiles);
                bwCopyDates.ReportProgress(progressValue);
                i++;
            }
        }

        private void bwCopyDates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwCopyDates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwCopyDates_RunWorkerCompleted_FinishMessage_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwCopyDates_RunWorkerCompleted_FinishMessage_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
