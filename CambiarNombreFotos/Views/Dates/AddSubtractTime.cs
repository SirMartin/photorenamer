﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Dates
{
    public partial class AddSubtractTime : LocalizableForm, IPhotoToolsForm
    {
        public AddSubtractTime()
        {
            InitializeComponent();

            ResetDateValues();

            this.folderBrowserUserControl.FolderChanged += new EventHandler(FolderBrowserUserControl_FolderChanged);
        }

        private void FolderBrowserUserControl_FolderChanged(object sender, EventArgs e)
        {
            LoadNumberOfFilesLabels();
        }

        private void LoadNumberOfFilesLabels()
        {
            folderContentUserControl.CalculateContent(folderBrowserUserControl.SelectedFolder);
        }

        #region Progress Bar Adjust Dates

        private void bwAdjustDates_DoWork(object sender, DoWorkEventArgs e)
        {
            var files = FileHelper.GetMultimediaFiles(folderBrowserUserControl.SelectedFolder);
            var totalFiles = files.Count();
            var i = 1;
            foreach (var filepath in files)
            {
                // Get add/subtract values.
                var days = Convert.ToInt32(txtDays.Text);
                var hours = Convert.ToInt32(txtHours.Text);
                var minutes = Convert.ToInt32(txtMinutes.Text);
                var seconds = Convert.ToInt32(txtSeconds.Text);

                // Check if its add or subtract.
                if (!rbnAdd.Checked)
                {
                    days = days * -1;
                    hours = hours * -1;
                    minutes = minutes * -1;
                    seconds = seconds * -1;
                }

                // Change date picture taken.
                if (chkTakenDate.Checked)
                {
                    var datetimeMetadata = MetadataHelper.GetMetadataDate(filepath);
                    datetimeMetadata = datetimeMetadata?.AddDays(days).AddHours(hours).AddMinutes(minutes).AddSeconds(seconds) ?? File.GetCreationTime(filepath);
                    MetadataHelper.ChangeDateInMetadata(filepath, datetimeMetadata.Value);
                }

                // Change creation datetime.
                if (chkCreationDate.Checked)
                {
                    var datetimeCreation = File.GetCreationTime(filepath);
                    datetimeCreation =
                        datetimeCreation.AddDays(days).AddHours(hours).AddMinutes(minutes).AddSeconds(seconds);
                    File.SetCreationTime(filepath, datetimeCreation);
                }

                // Change modification datetime.
                if (chkModificationDate.Checked)
                {
                    var datetimeModified = File.GetLastWriteTime(filepath);
                    datetimeModified =
                        datetimeModified.AddDays(days).AddHours(hours).AddMinutes(minutes).AddSeconds(seconds);
                    File.SetLastWriteTime(filepath, datetimeModified);
                }

                var progressValue = (i * 100 / totalFiles);
                bwAdjustDates.ReportProgress(progressValue);
                i++;
            }
        }

        private void bwAdjustDates_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
        }

        private void bwAdjustDates_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(LocalResourceManager.GetString("bwAdjustDates_RunWorkerCompleted_AdjustmentFinished_Text", CultureInfo.CurrentCulture),
                LocalResourceManager.GetString("bwAdjustDates_RunWorkerCompleted_AdjustmentFinished_Title", CultureInfo.CurrentCulture),
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion

        #region Inits

        private void ResetDateValues()
        {
            // Set default values in txtbox.
            txtDays.Text = "0";
            txtHours.Text = "0";
            txtMinutes.Text = "0";
            txtSeconds.Text = "0";
        }

        #endregion

        #region Ajustar Fechas

        private void btnAdjustDate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(folderBrowserUserControl.SelectedFolder))
            {
                // Error directory is empty.
                MessageBox.Show(LocalResourceManager.GetString("btnAdjustDate_Click_FolderNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnAdjustDate_Click_FolderNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!chkTakenDate.Checked && !chkCreationDate.Checked && !chkModificationDate.Checked)
            {
                // Needs to select at least one target.
                MessageBox.Show(LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Text", CultureInfo.CurrentCulture),
                    LocalResourceManager.GetString("btnCopyDates_Click_TargetDateNotSelected_Title", CultureInfo.CurrentCulture),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bwAdjustDates.RunWorkerAsync();
            }
        }

        #endregion

        #region Utilidades

        private void onlyNumericInTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Force only write numbers in the fields.
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '-')
            {
                // Check if only starts with.
                if (string.IsNullOrEmpty(((TextBox)sender).Text))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
            else if (Char.IsControl(e.KeyChar)) // Accept control keys (enter, ctrl, ...)
            {
                e.Handled = false;
            }
            else
            {
                // Rest of the keys are deactivated.
                e.Handled = true;
            }
        }

        #endregion

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
