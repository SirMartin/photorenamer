﻿using System.Drawing;
using System.Windows.Forms;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;

namespace PiribiPhotoTools.Views.Dates
{
    public partial class ZoomPictures : Form, IPhotoToolsForm
    {
        public ZoomPictures()
        {
            InitializeComponent();
        }

        public ZoomPictures(string leftImagePath, string rightImagePath)
        {
            InitializeComponent();

            // Set left image if exits.
            if (!string.IsNullOrEmpty(leftImagePath))
            {
                pictureBoxLeft.Image = Image.FromFile(leftImagePath);
                pictureBoxLeft.SizeMode = PictureBoxSizeMode.Zoom;
            }

            // Set right image if exits.
            if (!string.IsNullOrEmpty(rightImagePath))
            {
                pictureBoxRight.Image = Image.FromFile(rightImagePath);
                pictureBoxRight.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        public void ChangeLanguage(string lang)
        {
            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);
        }
    }
}
