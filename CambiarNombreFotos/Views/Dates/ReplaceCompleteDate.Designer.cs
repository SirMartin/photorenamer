﻿namespace PiribiPhotoTools.Views.Dates
{
    partial class ReplaceCompleteDate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplaceCompleteDate));
            this.progressBar = new PiribiPhotoTools.Custom.LabeledProgressBar();
            this.bwAdjustDates = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkTakenDate = new System.Windows.Forms.CheckBox();
            this.chkCreationDate = new System.Windows.Forms.CheckBox();
            this.chkModificationDate = new System.Windows.Forms.CheckBox();
            this.btnAdjustDate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panModificarFecha = new System.Windows.Forms.Panel();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.folderContentUserControl = new PiribiPhotoTools.Views.Shared.FolderContentUserControl();
            this.folderBrowserUserControl = new PiribiPhotoTools.Views.Shared.FolderBrowserUserControl();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panModificarFecha.SuspendLayout();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.CustomText = null;
            this.progressBar.DisplayStyle = PiribiPhotoTools.Enums.ProgressBarDisplayText.Percentage;
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // bwAdjustDates
            // 
            this.bwAdjustDates.WorkerReportsProgress = true;
            this.bwAdjustDates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwAdjustDates_DoWork);
            this.bwAdjustDates.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwAdjustDates_ProgressChanged);
            this.bwAdjustDates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwAdjustDates_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkTakenDate);
            this.panel1.Controls.Add(this.chkCreationDate);
            this.panel1.Controls.Add(this.chkModificationDate);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkTakenDate
            // 
            resources.ApplyResources(this.chkTakenDate, "chkTakenDate");
            this.chkTakenDate.Name = "chkTakenDate";
            this.chkTakenDate.UseVisualStyleBackColor = true;
            // 
            // chkCreationDate
            // 
            resources.ApplyResources(this.chkCreationDate, "chkCreationDate");
            this.chkCreationDate.Name = "chkCreationDate";
            this.chkCreationDate.UseVisualStyleBackColor = true;
            // 
            // chkModificationDate
            // 
            resources.ApplyResources(this.chkModificationDate, "chkModificationDate");
            this.chkModificationDate.Name = "chkModificationDate";
            this.chkModificationDate.UseVisualStyleBackColor = true;
            // 
            // btnAdjustDate
            // 
            resources.ApplyResources(this.btnAdjustDate, "btnAdjustDate");
            this.btnAdjustDate.Name = "btnAdjustDate";
            this.btnAdjustDate.UseVisualStyleBackColor = true;
            this.btnAdjustDate.Click += new System.EventHandler(this.btnAdjustDate_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.panModificarFecha);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panModificarFecha
            // 
            this.panModificarFecha.BackColor = System.Drawing.Color.Transparent;
            this.panModificarFecha.Controls.Add(this.datePicker);
            resources.ApplyResources(this.panModificarFecha, "panModificarFecha");
            this.panModificarFecha.Name = "panModificarFecha";
            // 
            // datePicker
            // 
            resources.ApplyResources(this.datePicker, "datePicker");
            this.datePicker.Name = "datePicker";
            // 
            // folderContentUserControl
            // 
            this.folderContentUserControl.ImagesAmount = 0;
            resources.ApplyResources(this.folderContentUserControl, "folderContentUserControl");
            this.folderContentUserControl.Name = "folderContentUserControl";
            this.folderContentUserControl.VideosAmount = 0;
            // 
            // folderBrowserUserControl
            // 
            resources.ApplyResources(this.folderBrowserUserControl, "folderBrowserUserControl");
            this.folderBrowserUserControl.Name = "folderBrowserUserControl";
            this.folderBrowserUserControl.SelectedFolder = null;
            // 
            // ReplaceCompleteDate
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.folderBrowserUserControl);
            this.Controls.Add(this.folderContentUserControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAdjustDate);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.progressBar);
            this.Icon = global::PiribiPhotoTools.Properties.Resources.photorenamer;
            this.Name = "ReplaceCompleteDate";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panModificarFecha.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Custom.LabeledProgressBar progressBar;
        private System.ComponentModel.BackgroundWorker bwAdjustDates;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkTakenDate;
        private System.Windows.Forms.CheckBox chkCreationDate;
        private System.Windows.Forms.CheckBox chkModificationDate;
        private System.Windows.Forms.Button btnAdjustDate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panModificarFecha;
        private System.Windows.Forms.DateTimePicker datePicker;
        private Shared.FolderContentUserControl folderContentUserControl;
        private Shared.FolderBrowserUserControl folderBrowserUserControl;
    }
}

