﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using PiribiPhotoTools.Enums;
using PiribiPhotoTools.Helpers;
using PiribiPhotoTools.Interfaces;
using PiribiPhotoTools.Properties;
using PiribiPhotoTools.Services;
using PiribiPhotoTools.Views.Basic;
using PiribiPhotoTools.Views.Dates;
using PiribiPhotoTools.Views.Info;
using Settings = PiribiPhotoTools.Views.Info.Settings;

namespace PiribiPhotoTools.Views
{
    public partial class Home : LocalizableForm, IPhotoToolsForm
    {
        public Home()
        {
            InitializeComponent();

            DisableUnfinishedFeatures();

            MaximizeBox = false;

            InitializeLanguage();

            // Launch Welcome Spot
            OpenFormInPanel(ApplicationForms.WelcomeSpot);
        }

        private void DisableUnfinishedFeatures()
        {
            autoCollectionAdjustmentToolStripMenuItem.Enabled = false;
            exifGeoTaggingToolStripMenuItem.Enabled = false;
            autoRotationToolStripMenuItem.Enabled = false;
            photoAsRefAdjustmentToolStripMenuItem.Enabled = false;
            helpToolStripMenuItem.Enabled = false;
        }

        private void InitializeLanguage()
        {
            var ci = CultureInfo.InstalledUICulture;
            if (ci.TwoLetterISOLanguageName.ToLower() == "es")
            {
                ChangeLanguage("es-es");
            }
            else
            {
                ChangeLanguage("en");
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.Home_salirToolStripMenuItem_Click_, Resources.Home_salirToolStripMenuItem_Click_Salir, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        #region Language

        private void spanishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeLanguage("es-es");
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeLanguage("en");
        }

        private void DisableAllCheckboxLanguages()
        {
            englishToolStripMenuItem.Checked = false;
            spanishToolStripMenuItem.Checked = false;
        }

        public void ChangeLanguage(string lang)
        {
            DisableAllCheckboxLanguages();

            switch (lang)
            {
                case "es-es":
                    spanishToolStripMenuItem.Checked = true;
                    break;
                case "en":
                default:
                    englishToolStripMenuItem.Checked = true;
                    break;
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);

            var resourceManager = new ComponentResourceManager(typeof(Home));

            // Change menu strip language.
            var myObjects = new List<object>
            {
                utilitiesToolStripMenuItem,
                aToolStripMenuItem,
                aboutToolStripMenuItem,
                helpToolStripMenuItem,
                renamePhotosToolStripMenuItem,
                autoRotationToolStripMenuItem,
                fechasDeFotografíasToolStripMenuItem,
                manualDateAdjustmentToolStripMenuItem,
                changeFullDateToolStripMenuItem,
                autoCollectionAdjustmentToolStripMenuItem,
                photoAsRefAdjustmentToolStripMenuItem,
                languageToolStripMenuItem,
                spanishToolStripMenuItem,
                englishToolStripMenuItem,
                exitToolStripMenuItem,
                exifGeoTaggingToolStripMenuItem,
                copyDatesToolStripMenuItem,
                copyDatesFromNameToolStripMenuItem,
                duplicateFilesToolStripMenuItem,
                settingsToolStripMenuItem,
                convertHeicToolStripMenuItem,
                checkForUpdatesToolStripMenuItem
            };

            foreach (var obj in myObjects)
            {
                resourceManager.ApplyResources(obj, ((ToolStripMenuItem)obj).Name, new CultureInfo(lang));
            }

            // Change labels language.
            LanguageHelper.ChangeLanguageControls(Controls, lang);

            // Reopen the same form.
            if (homePanel.Tag != null)
            {
                OpenFormInPanel((ApplicationForms)homePanel.Tag);
            }
        }

        #endregion

        private void OpenFormInPanel(ApplicationForms formType)
        {
            if (homePanel.Controls.Count > 0)
            {
                homePanel.Controls.RemoveAt(0);
            }

            var f = GetFormFromType(formType);

            f.FormClosed += new FormClosedEventHandler(Form_Closed);

            f.TopLevel = false;
            f.FormBorderStyle = FormBorderStyle.None;
            f.Dock = DockStyle.Fill;
            homePanel.Controls.Add(f);
            homePanel.Tag = formType;
            f.Show();
        }

        private Form GetFormFromType(ApplicationForms formType)
        {
            switch (formType)
            {
                case ApplicationForms.Basic_RenamePhotos:
                    return new RenamePhotos();
                case ApplicationForms.Basic_AutoRotation:
                    return new AutoRotateImages();
                case ApplicationForms.Basic_ExifGeoTagging:
                    return new ExifGeoTagging();
                case ApplicationForms.Basic_ConvertHeicPhotos:
                    return new ConvertHeicPhotos();
                case ApplicationForms.Dates_AddSubtractTime:
                    return new AddSubtractTime();
                case ApplicationForms.Dates_ReplaceCompleteDate:
                    return new ReplaceCompleteDate();
                case ApplicationForms.Dates_CopyDates:
                    return new CopyDates();
                case ApplicationForms.Dates_CopyDatesFromNameFormat:
                    return new CopyDatesFromName();
                case ApplicationForms.Dates_AutoCollectionAdjustment:
                    return new AutomaticDateSettingsForCollections();
                case ApplicationForms.Information_About:
                    return new About();
                case ApplicationForms.Duplicate_Files:
                    return new DuplicateFiles();
                case ApplicationForms.Settings:
                    return new Settings();
                case ApplicationForms.WelcomeSpot:
                    return new WelcomeSpot();
                default:
                    return null;
            }
        }

        private void renamePhotosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<RenamePhotos>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Basic_RenamePhotos);
        }

        private void convertHeicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<ConvertHeicPhotos>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Basic_ConvertHeicPhotos);
        }

        private void automaticDateSettingsForCollectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<AutomaticDateSettingsForCollections>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Dates_AutoCollectionAdjustment);
        }

        private void autoRotateImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<AutoRotateImages>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Basic_AutoRotation);
        }

        private void exifGeoTaggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<ExifGeoTagging>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Basic_ExifGeoTagging);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<About>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Information_About);
        }

        private void copyDatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<CopyDates>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Dates_CopyDates);
        }

        private void addSubtractTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<AddSubtractTime>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Dates_AddSubtractTime);
        }

        private void replaceCompleteDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<ReplaceCompleteDate>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Dates_ReplaceCompleteDate);
        }

        private void copyDatesFromNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<CopyDatesFromName>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Dates_CopyDatesFromNameFormat);
        }

        private void duplicateFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<DuplicateFiles>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Duplicate_Files);
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = Application.OpenForms.OfType<Settings>().FirstOrDefault();
            if (form != null)
            {
                form.Dispose();
            }

            OpenFormInPanel(ApplicationForms.Settings);
        }

        void Form_Closed(object sender, FormClosedEventArgs e)
        {
            Form frm = (Form)sender;

            if (frm.GetType() == typeof(Settings))
            {
                // Closing the settings.
                OpenFormInPanel(ApplicationForms.WelcomeSpot);
            }
        }

        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var updatesManager = new UpdatesManager();
            updatesManager.ManualCheckForUpdates();
        }

        private void logsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Piribi PhotoTools", "logs"));
        }
    }
}
