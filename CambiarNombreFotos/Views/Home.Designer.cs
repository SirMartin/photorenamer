﻿namespace PiribiPhotoTools.Views
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.utilitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renamePhotosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoRotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exifGeoTaggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertHeicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fechasDeFotografíasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualDateAdjustmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeFullDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyDatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyDatesFromNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.autoCollectionAdjustmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.photoAsRefAdjustmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spanishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homePanel = new System.Windows.Forms.Panel();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.utilitiesToolStripMenuItem,
            this.fechasDeFotografíasToolStripMenuItem,
            this.aToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // utilitiesToolStripMenuItem
            // 
            this.utilitiesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renamePhotosToolStripMenuItem,
            this.autoRotationToolStripMenuItem,
            this.exifGeoTaggingToolStripMenuItem,
            this.duplicateFilesToolStripMenuItem,
            this.convertHeicToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.utilitiesToolStripMenuItem.Name = "utilitiesToolStripMenuItem";
            resources.ApplyResources(this.utilitiesToolStripMenuItem, "utilitiesToolStripMenuItem");
            // 
            // renamePhotosToolStripMenuItem
            // 
            this.renamePhotosToolStripMenuItem.Name = "renamePhotosToolStripMenuItem";
            resources.ApplyResources(this.renamePhotosToolStripMenuItem, "renamePhotosToolStripMenuItem");
            this.renamePhotosToolStripMenuItem.Click += new System.EventHandler(this.renamePhotosToolStripMenuItem_Click);
            // 
            // autoRotationToolStripMenuItem
            // 
            resources.ApplyResources(this.autoRotationToolStripMenuItem, "autoRotationToolStripMenuItem");
            this.autoRotationToolStripMenuItem.Name = "autoRotationToolStripMenuItem";
            this.autoRotationToolStripMenuItem.Click += new System.EventHandler(this.autoRotateImagesToolStripMenuItem_Click);
            // 
            // exifGeoTaggingToolStripMenuItem
            // 
            this.exifGeoTaggingToolStripMenuItem.Name = "exifGeoTaggingToolStripMenuItem";
            resources.ApplyResources(this.exifGeoTaggingToolStripMenuItem, "exifGeoTaggingToolStripMenuItem");
            this.exifGeoTaggingToolStripMenuItem.Click += new System.EventHandler(this.exifGeoTaggingToolStripMenuItem_Click);
            // 
            // duplicateFilesToolStripMenuItem
            // 
            this.duplicateFilesToolStripMenuItem.Name = "duplicateFilesToolStripMenuItem";
            resources.ApplyResources(this.duplicateFilesToolStripMenuItem, "duplicateFilesToolStripMenuItem");
            this.duplicateFilesToolStripMenuItem.Click += new System.EventHandler(this.duplicateFilesToolStripMenuItem_Click);
            // 
            // convertHeicToolStripMenuItem
            // 
            this.convertHeicToolStripMenuItem.Name = "convertHeicToolStripMenuItem";
            resources.ApplyResources(this.convertHeicToolStripMenuItem, "convertHeicToolStripMenuItem");
            this.convertHeicToolStripMenuItem.Click += new System.EventHandler(this.convertHeicToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // fechasDeFotografíasToolStripMenuItem
            // 
            this.fechasDeFotografíasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualDateAdjustmentToolStripMenuItem,
            this.changeFullDateToolStripMenuItem,
            this.copyDatesToolStripMenuItem,
            this.copyDatesFromNameToolStripMenuItem,
            this.toolStripSeparator1,
            this.autoCollectionAdjustmentToolStripMenuItem,
            this.photoAsRefAdjustmentToolStripMenuItem});
            this.fechasDeFotografíasToolStripMenuItem.Name = "fechasDeFotografíasToolStripMenuItem";
            resources.ApplyResources(this.fechasDeFotografíasToolStripMenuItem, "fechasDeFotografíasToolStripMenuItem");
            // 
            // manualDateAdjustmentToolStripMenuItem
            // 
            this.manualDateAdjustmentToolStripMenuItem.Name = "manualDateAdjustmentToolStripMenuItem";
            resources.ApplyResources(this.manualDateAdjustmentToolStripMenuItem, "manualDateAdjustmentToolStripMenuItem");
            this.manualDateAdjustmentToolStripMenuItem.Click += new System.EventHandler(this.addSubtractTimeToolStripMenuItem_Click);
            // 
            // changeFullDateToolStripMenuItem
            // 
            this.changeFullDateToolStripMenuItem.Name = "changeFullDateToolStripMenuItem";
            resources.ApplyResources(this.changeFullDateToolStripMenuItem, "changeFullDateToolStripMenuItem");
            this.changeFullDateToolStripMenuItem.Click += new System.EventHandler(this.replaceCompleteDateToolStripMenuItem_Click);
            // 
            // copyDatesToolStripMenuItem
            // 
            this.copyDatesToolStripMenuItem.Name = "copyDatesToolStripMenuItem";
            resources.ApplyResources(this.copyDatesToolStripMenuItem, "copyDatesToolStripMenuItem");
            this.copyDatesToolStripMenuItem.Click += new System.EventHandler(this.copyDatesToolStripMenuItem_Click);
            // 
            // copyDatesFromNameToolStripMenuItem
            // 
            this.copyDatesFromNameToolStripMenuItem.Name = "copyDatesFromNameToolStripMenuItem";
            resources.ApplyResources(this.copyDatesFromNameToolStripMenuItem, "copyDatesFromNameToolStripMenuItem");
            this.copyDatesFromNameToolStripMenuItem.Click += new System.EventHandler(this.copyDatesFromNameToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // autoCollectionAdjustmentToolStripMenuItem
            // 
            resources.ApplyResources(this.autoCollectionAdjustmentToolStripMenuItem, "autoCollectionAdjustmentToolStripMenuItem");
            this.autoCollectionAdjustmentToolStripMenuItem.Name = "autoCollectionAdjustmentToolStripMenuItem";
            this.autoCollectionAdjustmentToolStripMenuItem.Click += new System.EventHandler(this.automaticDateSettingsForCollectionsToolStripMenuItem_Click);
            // 
            // photoAsRefAdjustmentToolStripMenuItem
            // 
            resources.ApplyResources(this.photoAsRefAdjustmentToolStripMenuItem, "photoAsRefAdjustmentToolStripMenuItem");
            this.photoAsRefAdjustmentToolStripMenuItem.Name = "photoAsRefAdjustmentToolStripMenuItem";
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkForUpdatesToolStripMenuItem,
            this.languageToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.logsToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            resources.ApplyResources(this.aToolStripMenuItem, "aToolStripMenuItem");
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            resources.ApplyResources(this.checkForUpdatesToolStripMenuItem, "checkForUpdatesToolStripMenuItem");
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem_Click);
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spanishToolStripMenuItem,
            this.englishToolStripMenuItem});
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            resources.ApplyResources(this.languageToolStripMenuItem, "languageToolStripMenuItem");
            // 
            // spanishToolStripMenuItem
            // 
            this.spanishToolStripMenuItem.Checked = true;
            this.spanishToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.spanishToolStripMenuItem.Name = "spanishToolStripMenuItem";
            resources.ApplyResources(this.spanishToolStripMenuItem, "spanishToolStripMenuItem");
            this.spanishToolStripMenuItem.Click += new System.EventHandler(this.spanishToolStripMenuItem_Click);
            // 
            // englishToolStripMenuItem
            // 
            this.englishToolStripMenuItem.Name = "englishToolStripMenuItem";
            resources.ApplyResources(this.englishToolStripMenuItem, "englishToolStripMenuItem");
            this.englishToolStripMenuItem.Click += new System.EventHandler(this.englishToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            resources.ApplyResources(this.settingsToolStripMenuItem, "settingsToolStripMenuItem");
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            // 
            // homePanel
            // 
            resources.ApplyResources(this.homePanel, "homePanel");
            this.homePanel.Name = "homePanel";
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            resources.ApplyResources(this.logsToolStripMenuItem, "logsToolStripMenuItem");
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click);
            // 
            // Home
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.homePanel);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Home";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem utilitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renamePhotosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoRotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fechasDeFotografíasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualDateAdjustmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeFullDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem autoCollectionAdjustmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem photoAsRefAdjustmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spanishToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel homePanel;
        private System.Windows.Forms.ToolStripMenuItem exifGeoTaggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyDatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyDatesFromNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem duplicateFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertHeicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
    }
}