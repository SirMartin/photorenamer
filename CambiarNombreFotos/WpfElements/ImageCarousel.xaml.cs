// -------------------------------------------------------------------------------
// 
// This file is part of the FluidKit project: http://www.codeplex.com/fluidkit
// 
// Copyright (c) 2008, The FluidKit community 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, 
// are permitted provided that the following conditions are met:
// 
// * Redistributions of source code must retain the above copyright notice, this 
// list of conditions and the following disclaimer.
// 
// * Redistributions in binary form must reproduce the above copyright notice, this 
// list of conditions and the following disclaimer in the documentation and/or 
// other materials provided with the distribution.
// 
// * Neither the name of FluidKit nor the names of its contributors may be used to 
// endorse or promote products derived from this software without specific prior 
// written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
// -------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using FluidKit.Controls;

namespace PiribiPhotoTools.WpfElements
{
    public partial class ImageCarousel
    {
        private StringCollection _dataSource;

        private Random _randomizer = new Random();

        public string FilePath { get; set; }

        public ImageCarousel(string filepath)
        {
            FilePath = filepath;

            InitializeComponent();
            Loaded += Window1_Loaded;
        }

        private void Window1_Loaded(object sender, RoutedEventArgs e)
        {
            _elementFlow.Layout = new Wall(); //new Rolodex();
            _elementFlow.SelectedIndex = 0;

            // Configuration of perspective.
            _elementFlow.TiltAngle = 45; //Min 0 - Max 90
            _elementFlow.ItemGap = 0.65; //Min 0.25 - Max 3
            _elementFlow.FrontItemGap = 0; //Min 0 - Max 4
            _elementFlow.PopoutDistance = 0.5; //Min -2 - Max 2

            _dataSource = FindResource("DataSource") as StringCollection;

            // Load Images
            LoadImages();
        }

        private void LoadImages()
        {
            if (!string.IsNullOrEmpty(FilePath))
            {
                var files = Directory.GetFiles(FilePath, "*.jpg", SearchOption.TopDirectoryOnly);
                foreach (var file in files)
                {
                    var index = _dataSource.Count;
                    _dataSource.Insert(index, file);
                }
            }
            else
            {
                _dataSource = null;
            }
        }

    }

    public class StringCollection : ObservableCollection<string>
    {
        public StringCollection()
        {
        }
    }
}