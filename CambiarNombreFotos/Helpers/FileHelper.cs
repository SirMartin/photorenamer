﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PiribiPhotoTools.Helpers
{
    public class FileHelper
    {
        public static readonly string[] MetadataImageTypes = { ".jpg", ".jpeg", ".heic" };
        public static readonly string[] HeicImageTypes = { ".heic" };
        public static readonly string[] ImageTypes = { ".jpg", ".jpeg", ".png", ".gif", ".tiff", ".bmp", ".heic" };
        public static readonly string[] VideoTypes = { ".mov", ".avi", ".mpg", ".mp4", ".mkv", ".3gp" };

        #region Check file types

        /// <summary>
        /// Check if the file is a JPG or HEIC file.
        /// </summary>
        /// <param name="filepath">The path of the file.</param>
        /// <returns>If its JPG or HEIC file or not.</returns>
        public static bool IsImageWithMetadataFile(string filepath)
        {
            var filters = MetadataImageTypes;
            var extension = Path.GetExtension(filepath);
            return (extension != null && filters.Contains(extension.ToLower()));
        }

        /// <summary>
        /// Check if the file is a picture file.
        /// </summary>
        /// <param name="filepath">The path of the file.</param>
        /// <returns>If its a picture file or not.</returns>
        public static bool IsImageFile(string filepath)
        {
            var filters = ImageTypes;
            var extension = Path.GetExtension(filepath);
            return (extension != null && filters.Contains(extension.ToLower()));
        }

        /// <summary>
        /// Check if the file is a video file.
        /// </summary>
        /// <param name="filepath">The path of the file.</param>
        /// <returns>If its a video file or not.</returns>
        public static bool IsVideoFile(string filepath)
        {
            var filters = VideoTypes;
            var extension = Path.GetExtension(filepath);
            return (extension != null && filters.Contains(extension.ToLower()));
        }

        /// <summary>
        /// Check if the file is a HEIC file.
        /// </summary>
        /// <param name="filepath">The path of the file.</param>
        /// <returns>If its a HEIC picture file or not.</returns>
        public static bool IsHeicImageFile(string filepath)
        {
            var filters = HeicImageTypes;
            var extension = Path.GetExtension(filepath);
            return (extension != null && filters.Contains(extension.ToLower()));
        }

        /// <summary>
        /// Check if the file is a video or a picture.
        /// </summary>
        /// <param name="filepath">The path of the file.</param>
        /// <returns>If its a multimedia file or not.</returns>
        public static bool IsMultimediaFile(string filepath)
        {
            return (IsImageFile(filepath) || IsVideoFile(filepath));
        }

        #endregion

        #region Get File Types

        /// <summary>
        /// Return all the image files from a selected folder, and maybe also all the child folders.
        /// </summary>
        /// <param name="searchFolder">The path of the folder to search.</param>
        /// <param name="isRecursive">To search also in all the child folders. False by default.</param>
        /// <returns>The list of the pictures paths.</returns>
        public static IEnumerable<string> GetImageFilesFrom(string searchFolder, bool isRecursive = false)
        {
            var filters = ImageTypes;
            return GetFiles(searchFolder, filters, isRecursive);
        }

        /// <summary>
        /// Return all the video files from a selected folder, and maybe also all the child folders.
        /// </summary>
        /// <param name="searchFolder">The path of the folder to search.</param>
        /// <param name="isRecursive">To search also in all the child folders. False by default.</param>
        /// <returns>The list of the video paths.</returns>
        public static IEnumerable<string> GetVideoFilesFrom(string searchFolder, bool isRecursive = false)
        {
            var filters = VideoTypes;
            return GetFiles(searchFolder, filters, isRecursive);
        }

        /// <summary>
        /// Return all the multimedia files (pictures and videos) from a selected folder, and maybe also all the child folders.
        /// </summary>
        /// <param name="searchFolder">The path of the folder to search.</param>
        /// <param name="isRecursive">To search also in all the child folders. False by default.</param>
        /// <returns>The list of the multimedia files path.</returns>
        public static IEnumerable<string> GetMultimediaFiles(string searchFolder, bool isRecursive = false)
        {
            var filePaths = new List<string>();
            filePaths.AddRange(GetImageFilesFrom(searchFolder, isRecursive));
            filePaths.AddRange(GetVideoFilesFrom(searchFolder, isRecursive));
            return filePaths;
        }

        /// <summary>
        /// Return all the files from a selected folder for the selected filter, and maybe also all the child folders.
        /// </summary>
        /// <param name="searchFolder">The path of the folder to search.</param>
        /// <param name="filters">The desired file types.</param>
        /// <param name="isRecursive">To search also in all the child folders. False by default.</param>
        /// <returns>The list of the founded file paths.</returns>
        private static IEnumerable<string> GetFiles(string searchFolder, string[] filters, bool isRecursive = false)
        {
            var filesFound = new List<string>();
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, $"*{filter}", searchOption));
            }
            return filesFound.ToArray();
        }

        #endregion
    }
}
