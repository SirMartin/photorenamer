﻿using System;
using System.Globalization;
using System.IO;
using Shell32;
using System.Windows.Media.Imaging;
using System.Threading;
using NLog;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Microsoft.WindowsAPICodePack.Shell;

namespace PiribiPhotoTools.Helpers
{
    public class MetadataHelper
    {
        public Logger Logger => LogManager.GetLogger("MetadataHelper");

        private int? _osColumnId = null;
        public int OsColumnId
        {
            get
            {
                if (!_osColumnId.HasValue)
                {
                    _osColumnId = GetColumnIdForOsVersion();
                }

                return _osColumnId.Value;
            }
        }

        public static void SetVideoItemDate(string filepath, DateTime date)
        {
            var file = ShellFile.FromFilePath(filepath);

            ShellPropertyWriter propertyWriter = file.Properties.GetPropertyWriter();
            propertyWriter.WriteProperty(SystemProperties.System.ItemDate, date);
            propertyWriter.Close();
        }

        private static DateTime? GetVideoItemDate(string filepath)
        {
            var file = ShellFile.FromFilePath(filepath);

            return file.Properties.System.ItemDate.Value;
        }

        [Obsolete("Should use GetVideoItemDate instead.")]
        /// <summary>
        /// Find the way to run from the background worker, and not from the Main Thread.
        /// https://stackoverflow.com/questions/31403956/exception-when-using-shell32-to-get-file-extended-properties
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private void GetVideoMediaCreationDate(object param)
        {
            var filePath = (string)param;
            try
            {
                var shell = new Shell();
                var folder = shell.NameSpace(Path.GetDirectoryName(filePath));
                var file = folder.ParseName(Path.GetFileName(filePath));

                // These are the characters that are not allowing me to parse into a DateTime
                var charactersToRemove = new[] {
                    (char)8206,
                    (char)8207
                };

                // Getting the "Media Created" label (don't really need this, but what the heck)
                //var name = folder.GetDetailsOf(null, OsColumnId);

                // Getting the "Media Created" value as a string
                var value = folder.GetDetailsOf(file, OsColumnId).Trim();

                // Removing the suspect characters
                foreach (var c in charactersToRemove)
                {
                    value = value.Replace((c).ToString(), "").Trim();
                }

                // If the value string is empty, return DateTime.MinValue, otherwise return the "Media Created" date
                MediaCreationDate = value == string.Empty ? (DateTime?)null : DateTime.Parse(value);
            }
            catch (Exception ex)
            {
                Logger.Error("Error Getting Video Media Creation Date", ex);
                //return null;
            }
        }

        private int GetColumnIdForOsVersion()
        {
            switch (Environment.OSVersion.Version.Major)
            {
                case 6:
                    if (Environment.OSVersion.Version.Minor < 2)
                    {
                        // Windows Vista or 7
                        return 191; // TODO: Needs to be checked
                    }
                    else
                    {
                        // Windows 8 or 8.1
                        return 177; // TODO: Needs to be checked
                    }
                case 10:
                default:
                    // Windows 10
                    return 208;
            }
        }

        public static DateTime? GetMetadataDate(string filepath)
        {
            if (FileHelper.IsImageWithMetadataFile(filepath))
            {
                return GetPictureTakenDateFromMetadata(filepath);
            }
            else if (FileHelper.IsVideoFile(filepath))
            {
                return GetVideoItemDate(filepath);
                //if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA)
                //{
                //    GetVideoMediaCreationDate(filepath);
                //}
                //else
                //{
                //    object args = filepath;
                //    var staThread = new Thread(new ParameterizedThreadStart(GetVideoMediaCreationDate));
                //    staThread.SetApartmentState(ApartmentState.STA);
                //    staThread.Start(args);
                //    staThread.Join();
                //}

                //return MediaCreationDate;
            }

            return null;
        }


        public DateTime? MediaCreationDate { get; set; }

        public static DateTime? GetPictureTakenDateFromMetadata(string filepath)
        {
            if (!FileHelper.IsImageWithMetadataFile(filepath))
            {
                return null;
            }

            var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read);
            var img = BitmapFrame.Create(fs);
            var md = (BitmapMetadata)img.Metadata;
            if (md == null || md.DateTaken == null)
                return null;
            var date = Convert.ToDateTime(md.DateTaken, CultureInfo.CurrentCulture);
            fs.Close();
            return date;
        }

        public static void ChangeDateInMetadata(string filepath, DateTime date)
        {
            if (FileHelper.IsImageWithMetadataFile(filepath))
            {
                ChangePictureDateInMetadata(filepath, date);
            }
            else if (FileHelper.IsVideoFile(filepath))
            {
                SetVideoItemDate(filepath, date);
            }
        }

        public static void ChangePictureDateInMetadata(string filepath, DateTime date)
        {
            string newfilepath = filepath + "_-_-_";
            // Get the source image stream
            using (var imageFileStream = new FileStream(filepath, FileMode.Open))
            {
                // Load the image in the decoder
                var decoder = new JpegBitmapDecoder(imageFileStream,
                    BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

                // Make a copy of the frame, this will also unlock the metadata
                BitmapFrame frameCopy = BitmapFrame.Create(decoder.Frames[0]);

                // Now we have a metadata object that is unfrozen
                BitmapMetadata copyMetadata = (BitmapMetadata)frameCopy.Metadata;

                // Set your metadata here, metadata in the source frame
                // will be rewritten to the output frame and any changes
                // will overwrite the metadata in the source frame.
                var t = date.ToUniversalTime().ToString("o");
                copyMetadata.DateTaken = t;

                // Create a new encoder and add the copied frame to it
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(frameCopy);

                // Save the new file with the new metadata
                using (FileStream imageFileOutStream = new FileStream(newfilepath, FileMode.Create))
                {
                    encoder.Save(imageFileOutStream);
                }
            }

            // Delete the previous file.
            File.Delete(filepath);

            // Rename the new one.
            File.Move(newfilepath, filepath);
        }
    }
}
