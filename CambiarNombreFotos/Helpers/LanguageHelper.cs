﻿using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using PiribiPhotoTools.Views;

namespace PiribiPhotoTools.Helpers
{
    public class LanguageHelper
    {
        public static void ChangeLanguageControls(Control.ControlCollection controls, string lang)
        {
            foreach (Control c in controls)
            {
                // Recursive call.
                ChangeLanguageControls(c.Controls, lang);

                var resources = new ComponentResourceManager(typeof(Home));
                resources.ApplyResources(c, c.Name, new CultureInfo(lang));
            }
        }
    }
}
