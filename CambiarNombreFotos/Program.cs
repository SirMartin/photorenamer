﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using PiribiPhotoTools.Views;
using NLog;
using NLog.Config;
using NLog.Targets;
using PiribiPhotoTools.Services;

namespace PiribiPhotoTools
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ConfigureNLog();

            // Language
            var ci = CultureInfo.InstalledUICulture;
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            // Manage updates.
            var dontCheckUpdates = Properties.Settings.Default.AutoUpdate;
            var updatesManager = new UpdatesManager();
            if (!dontCheckUpdates || updatesManager.CheckForUpdates())
            {
                var logger = LogManager.GetLogger("Program");
                logger.Info("Start Application");

                // Application
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Home());
            }
        }

        private static void ConfigureNLog()
        {
            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration 
            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            // Step 3. Set target properties 
            fileTarget.FileName = "${specialfolder:folder=LocalApplicationData}/Piribi PhotoTools/logs/log.txt";
            fileTarget.Layout = "${date:format=yyyyMMddHHmmss}|${level:uppercase=true}|${logger}|${message}";

            // Step 4. Define rules
            var rule1 = new LoggingRule("*", LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule1);

            // Step 5. Activate the configuration
            LogManager.Configuration = config;
        }

    }
}
