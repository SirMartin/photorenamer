﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Web;
using NLog;
using PiribiPhotoTools.Interfaces;
using PiribiPhotoTools.Models;

namespace PiribiPhotoTools.Services
{
    public class GeoApifyHelper : IGeoLocationApi
    {
        private const string BaseUrl = "https://api.geoapify.com/v1/geocode/";

        private const string ApiKey = "3919217d6cb34cd7b7d8ee1d81a8293c";

        private readonly UpdatesManager _updatesManager;

        public Logger Logger => LogManager.GetLogger("GeoApifyHelper");

        public GeoApifyHelper()
        {
            _updatesManager = new UpdatesManager();
        }

        public async Task<Coordinates> GetCoordinates(string address)
        {
            try
            {
                var encodedAddress = HttpUtility.UrlEncode(address);
                var url = $"{BaseUrl}search?text={encodedAddress}&apiKey={ApiKey}&format=json";

                HttpClient client = new HttpClient();
                
                var r = await client.GetFromJsonAsync<GeoLocationSearchResult>(url);
                                

                if (r.results.Any())
                {
                    return new Coordinates
                    {
                        Latitude = r.results[0].lat,
                        Longitude = r.results[0].lon,
                    };
                }

                return null;
            }
            catch (Exception ex)
            {
                //Log error
                Logger.Error($"GeoApify-GetCoordinates - {ex}");
                Logger.Error($"GeoApify-GetCoordinates - {ex.InnerException}");
                return null;
            }
        }
    }
}