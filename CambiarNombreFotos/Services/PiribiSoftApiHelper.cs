﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Windows.Forms;
using NLog;
using PiribiPhotoTools.Interfaces;
using PiribiPhotoTools.Properties;

namespace PiribiPhotoTools.Services
{
    public class PiribiSoftApiHelper : IPiribiSoftApiHelper
    {
        private const string BaseUrl = "https://api.piribisoft.com/phototools/";

        private readonly UpdatesManager _updatesManager;

        public Logger Logger => LogManager.GetLogger("PiribiSoftApiHelper");

        public PiribiSoftApiHelper()
        {
            _updatesManager = new UpdatesManager();
        }

        public bool ExistsUpdate(Version actualVersion)
        {
            try
            {
                var url = $"{BaseUrl}CheckUpdates?version={actualVersion}";
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                string content;
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            content = sr.ReadToEnd();
                        }
                    }
                }

                return bool.Parse(content);
            }
            catch (Exception ex)
            {
                //Log error
                Logger.Error($"Api-ExistsUpdate - {ex}");
                Logger.Error($"Api-ExistsUpdate - {ex.InnerException}");
                return false;
            }
        }

        public string GetNewestVersion()
        {
            try
            {
                var url = $"{BaseUrl}actualVersion";
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Method = "GET";
                string content;
                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            content = sr.ReadToEnd();
                        }
                    }
                }

                return content;
            }
            catch (Exception ex)
            {
                //Log error
                Logger.Error($"Api-ExistsUpdate - {ex}");
                Logger.Error($"Api-ExistsUpdate - {ex.InnerException}");
                return null;
            }
        }
        
        //https://stackoverflow.com/questions/35684243/how-to-download-and-run-a-exe-file-c-sharp
        public void DownloadInstaller(string filename)
        {
            var url = new Uri($"{BaseUrl}Download");
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
                else
                {
                    var wc = new WebClient();
                    wc.DownloadFileAsync(url, filename);
                    wc.DownloadFileCompleted += wc_DownloadFileCompleted(filename);
                }
            }
            catch (Exception ex)
            {
                //Log error
                Logger.Error($"Api-Download - {ex}");
                Logger.Error($"Api-Download - {ex.InnerException}");
            }
        }

        public AsyncCompletedEventHandler wc_DownloadFileCompleted(string filename)
        {
            Action<object, AsyncCompletedEventArgs> action = (sender, e) =>
            {
                if (e.Error == null)
                {
                    var dialogResult = MessageBox.Show(Resources.PiribiSoftApiHelper_UpdateReady_Text, Resources.PiribiSoftApiHelper_UpdateReady_Title, MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        _updatesManager.LaunchUpdate(filename);
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        //do something else
                    }
                }
                else
                {
                    Logger.Error($"Api-DownloadCompleted-Error{e.Error}");
                    Logger.Error($"Api-DownloadCompleted-Error{e.Error.InnerException}");
                }
            };
            return new AsyncCompletedEventHandler(action);
        }
    }
}