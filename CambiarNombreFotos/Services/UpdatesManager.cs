using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using PiribiPhotoTools.Properties;

namespace PiribiPhotoTools.Services
{
    public class UpdatesManager
    {
        private static readonly string UpdateFolderPath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Piribi PhotoTools");

        private static readonly string UpdateFilename = Path.Combine(UpdateFolderPath, "phototools-{0}-setup.msi");

        private const string UpdateFilenamePattern = "phototools-*-setup.msi";

        public void ManualCheckForUpdates()
        {
            var version = AssemblyName.GetAssemblyName("PiribiPhotoTools.exe").Version;
            var helper = new PiribiSoftApiHelper();
            var needsToUpdate = helper.ExistsUpdate(version);

            if (needsToUpdate)
            {
                var newestVersion = helper.GetNewestVersion();
                var filename = string.Format(UpdateFilename, newestVersion);
                helper.DownloadInstaller(filename);
            }
            else
            {
                MessageBox.Show(Resources.UpdatesManager_NoUpdateAvailable);
            }
        }

        public bool CheckForUpdates()
        {
            if (!Directory.Exists(UpdateFolderPath))
            {
                Directory.CreateDirectory(UpdateFolderPath);
            }

            var version = AssemblyName.GetAssemblyName("PiribiPhotoTools.exe").Version;
            var helper = new PiribiSoftApiHelper();
            var needsToUpdate = helper.ExistsUpdate(version);

            if (needsToUpdate)
            {
                var newestVersion = helper.GetNewestVersion();
                var filename = string.Format(UpdateFilename, newestVersion);
                // Check if file already exists.
                if (File.Exists(filename))
                {
                    // Make the update.
                    Process.Start(filename);
                    return false;
                }
                else
                {
                    // Download the update and show the popup, to make the user choice.
                    helper.DownloadInstaller(filename);
                }
            }
            else
            {
                // Remove all the old files.
                var updateFiles = Directory.GetFiles(UpdateFolderPath, UpdateFilenamePattern);
                foreach ( var updateFile in updateFiles )
                {
                    File.Delete(updateFile);
                }
            }

            return true;
        }

        public void LaunchUpdate(string filename)
        {
            // Launch update
            Process.Start(filename);

            // Close application.
            Application.Exit();
        }
    }
}
