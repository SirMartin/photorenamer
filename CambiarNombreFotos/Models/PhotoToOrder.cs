﻿using System;

namespace PiribiPhotoTools.Models
{
    public class PhotoToOrder
    {
        public DateTime? DateTime { get; set; }

        public string Filepath { get; set; }
    }
}
