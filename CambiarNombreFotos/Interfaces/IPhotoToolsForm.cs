﻿namespace PiribiPhotoTools.Interfaces
{
    public interface IPhotoToolsForm
    {
        void ChangeLanguage(string lang);
    }
}
