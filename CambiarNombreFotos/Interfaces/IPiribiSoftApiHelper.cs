﻿using System;

namespace PiribiPhotoTools.Interfaces
{
    public interface IPiribiSoftApiHelper
    {
        bool ExistsUpdate(Version actualVersion);

        string GetNewestVersion();

        void DownloadInstaller(string filename);
    }
}