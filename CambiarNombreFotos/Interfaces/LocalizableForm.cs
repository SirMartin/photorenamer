﻿using NLog;
using System.Resources;
using System.Windows.Forms;

namespace PiribiPhotoTools.Interfaces
{
    public class LocalizableForm : Form
    {
        public Logger Logger => LogManager.GetLogger(GetType().FullName);

        private ResourceManager _localResourceManager;

        public ResourceManager LocalResourceManager => _localResourceManager ?? (_localResourceManager = new ResourceManager(GetType()));
    }
}
