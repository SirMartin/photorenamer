﻿using PiribiPhotoTools.Models;
using System.Threading.Tasks;

namespace PiribiPhotoTools.Interfaces
{
    public interface IGeoLocationApi
    {
        Task<Coordinates> GetCoordinates(string address);
    }
}