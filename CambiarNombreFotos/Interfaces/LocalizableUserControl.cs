﻿using System.Resources;
using System.Windows.Forms;

namespace PiribiPhotoTools.Interfaces
{
    public class LocalizableUserControl : UserControl
    {
        private ResourceManager _localResourceManager;

        public ResourceManager LocalResourceManager => _localResourceManager ?? (_localResourceManager = new ResourceManager(GetType()));
    }
}
