﻿namespace PiribiPhotoTools.Enums
{
    public enum ApplicationForms
    {
        Basic_RenamePhotos,
        Basic_AutoRotation,
        Basic_ExifGeoTagging,
        Basic_ConvertHeicPhotos,
        Dates_AddSubtractTime,
        Dates_ReplaceCompleteDate,
        Dates_CopyDates,
        Dates_CopyDatesFromNameFormat,
        Dates_AutoCollectionAdjustment,
        Information_About,
        Duplicate_Files,
        Settings,
        WelcomeSpot
    }
}
