﻿namespace PiribiPhotoTools.Enums
{
    public enum ProgressBarDisplayText
    {
        Percentage,
        CustomText
    }
}
